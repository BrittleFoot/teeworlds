#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import SFML as sf
import src.tile_types as tile

from os.path import join
from src.level_tools import Level
from src.level_tools import TILESIZE
from src.level_tools import TYPE_COLOR
from src.level_tools import Tile
from src.smart_view import SmartView
from src.static_rect import StaticRect
from src.tile_types import TILESIZE
from src.util.mouse import Cursor

TYPE = {
    0: '             <- NOTHING :)',
    1: 'SOLID',
    2: 'THORNS',
    3: 'SPAWN',
    4: '             <- WEAP SPAWN :) Too bright, I knew c:'
}


def point_in_rect(rect, point):
    x = point.x >= rect.left and point.x <= rect.right
    y = point.y >= rect.top and point.y <= rect.bottom
    return x and y


def level_by_type_highlite(level, window):
    rect = sf.RectangleShape()
    rect.size = TILESIZE, TILESIZE

    for tile in level:
        rect.position = tile.position
        color = sf.Color(*TYPE_COLOR[tile.type])
        color.a = 70
        rect.fill_color = color
        window.draw(rect)


class TilePanel():

    """docstring for TilePanel"""

    def __init__(self, view, level):
        self.view = view
        self.tileset = level.tileset

        width, height = view.view.size

        w_size = width * 0.32
        h_size = height * 0.8

        x, y = width * 0.65, height * 0.15

        self.picked = 0

        self.frame = StaticRect.make(view, (x, y), (w_size, h_size))
        self.size_calc()

        self.plates = dict()

        for x in range(self.micro_size.x):
            for y in range(self.micro_size.y):
                tid = y * self.micro_size.x + x
                self.plates[x, y] = Tile(1, 0, (0, 0), tid)

        self.recolor_frame()

    def recolor_frame(self):
        from random import randint

        def rand(): return randint(0, 255)
        self.frame.fill_color = sf.Color(rand(), rand(), rand(), 50)

    def size_calc(self):
        self.frame.update(1)
        self.corner = self.frame.position + self.frame.size * 0.05
        self.size = self.frame.size * 0.9

        self.mytilesize = TILESIZE + 2

        x, y = self.size // self.mytilesize
        self.micro_size = sf.Vector2(int(x), int(y))

        self.rectangle = self.frame.global_bounds

    def update(self):
        self.size_calc()

        for pos, tile in self.plates.items():
            offset = sf.Vector2(*pos) * self.mytilesize
            tile.rect.position = self.corner + offset

    def check_mouse(self, mouse_pos):
        rect = sf.Rectangle(self.corner, self.size)
        if point_in_rect(rect, mouse_pos):
            pos = mouse_pos - self.corner
            pos = pos // self.mytilesize
            tile = self.plates.get(tuple(pos), None)
            if tile is not None:
                return tile.tid
        return None

    def draw(self, window):
        self.frame.draw(window)
        for tile in self.plates.values():
            self.tileset.apply_texture(tile)
            tile.draw(window)


def main():
    import sys
    if len(sys.argv) > 1:
        path = sys.argv[1]
    else:
        path = 'house'

    vmode = sf.VideoMode(1376, 768)
    vmode = sf.VideoMode(1920, 1080)

    style = sf.Style.TITLEBAR
    style = sf.Style.FULLSCREEN

    window = sf.RenderWindow(vmode, "Editor", style)
    window.framerate_limit = 60
    view = SmartView(vmode, window)
    view.view.size *= 2
    cursor = Cursor(window, vmode, view)

    level = Level.load_from_file(path)

    key = sf.Keyboard
    pressed = key.is_key_pressed

    if 1:

        rect = StaticRect.make(view, (100, 100), (200, 50))
        rect.fill_color = sf.Color.RED
        rect.outline_color = sf.Color.BLACK
        rect.outline_thickness = 2

        l, t = view.view.size.x - 300 - 100, 100
        status_rect = StaticRect.make(view, (l, t), (300, 50))
        status_rect.fill_color = sf.Color.RED

        font = sf.Font.from_file('src/fonts/monof55.ttf')
        text = sf.Text('TEXT', font)
        text.style = text.BOLD

        status = sf.Text('TEXT', font)
        text.style = text.BOLD

        rect.append(text)
        status_rect.append(status)

        tile_type = tile.NONE

        cursor_bounds = Tile(1, tile.NONE, (0, 0), 5)

        SCROLL_SPEED = 20
        TID = 0

    tilepanel = TilePanel(view, level)

    while window.is_open:
        mpressed = 0
        for event in window.events:
            if type(event) is sf.CloseEvent:
                window.close()

            if type(event) is sf.KeyEvent and event.pressed:
                if event.code >= key.NUM0 and event.code <= key.NUM0 + tile.WSPAWN + 1:
                    tile_type = event.code - key.NUM0 - 1
                if event.code == key.S and event.control:
                    Level.save(level)
                if event.code == key.B and event.control:
                    view.view.center = (0, 0)
                if event.code == key.LEFT and TID > 0:
                    TID -= 1
                if event.code == key.RIGHT and TID < level.tileset.max_tid:
                    TID += 1
                if event.code == key.UP and TID > level.tileset.x_count:
                    TID -= level.tileset.x_count
                if event.code == key.DOWN and TID < level.tileset.max_tid:
                    TID += level.tileset.x_count
                    TID = TID % level.tileset.max_tid
                if event.code == key.P:
                    tilepanel.recolor_frame()

            if type(event) is sf.MouseButtonEvent:
                mpressed = event.pressed

        if pressed(key.ADD):
            SCROLL_SPEED += 1
        if pressed(key.SUBTRACT) and SCROLL_SPEED > 0:
            SCROLL_SPEED -= 1

        if pressed(key.W):
            view.center -= 0, SCROLL_SPEED

        if pressed(key.S):
            view.center += 0, SCROLL_SPEED

        if pressed(key.A):
            view.center -= SCROLL_SPEED, 0

        if pressed(key.D):
            view.center += SCROLL_SPEED, 0

        view.update(1)
        cursor.update(1)
        rect.update(1)
        status_rect.update(1)
        tilepanel.update()

        mouse_pressed = sf.Mouse.is_button_pressed(sf.Mouse.LEFT)
        if mouse_pressed or pressed(key.SPACE):
            if not point_in_rect(tilepanel.rectangle, cursor.position):
                pos = cursor.position // TILESIZE * TILESIZE
                level[pos] = Tile(1, tile_type, pos, TID)

        if mpressed:
            tid_res = tilepanel.check_mouse(cursor.position)
            if tid_res:
                TID = tid_res

        rect.fill_color = TYPE_COLOR[tile_type]
        color = sf.Color(*TYPE_COLOR[tile_type])
        color.a = 100
        cursor_bounds.rect.fill_color = color
        cursor_bounds.rect.position = cursor.position // TILESIZE * TILESIZE
        cursor_bounds.tid = TID
        level.tileset.apply_texture(cursor_bounds)

        text.string = TYPE[tile_type]
        status.string = 'SCROLL SPEED: %s' % SCROLL_SPEED

        window.clear(sf.Color.BLACK)

        level.draw(window, not pressed(key.H))
        level_by_type_highlite(level, window)
        cursor_bounds.draw(window)
        rect.draw(window)
        status_rect.draw(window)
        tilepanel.draw(window)

        window.display()


if __name__ == '__main__':
    main()
