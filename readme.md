# __________________TEEWORLDS_README__________________ #


    ####################################################
    ##########==########################################
    ###########%=#########################%=@###########
    ############==########==@############%=#############
    #############%=%@*##%%@%%=#%@######@@%##############
    #############==*##%*:::::::::*###%+@@###############
    ###############:::::::::::::::::::#@@=##############
    ############+:::::::::::::::::::::::################
    ##########@:::::::::::::::::::::::::::##############
    #########*::::::::::::::::::=##::::::::###*:*#######
    ########+::::::::::::####::*####:::::::+#*::---*####
    ########:::::::::::::####:::####:::::::++#::::::*%##
    #######=::::::::::::::##-:::*##:::::::*++#++**+++###
    #######*::::::::::::::::::::::::::::::+++#######@#@#
    #######+:::::::::::::::::::::::::::::++++##@@@@@#@##
    ###%###::-*#%:::::=%+::::::::::::::*+++++#####%@@###
    ###%#:::----*#::::@@@%@+#=::::::::++++++############
    ####%+::::::*####%@@@@%%@%@%@::*+++++++#############
    #######%+++++#%%##@@%%%@%%@@##++++++++#%%###########
    #########@##%=%##@@%%%@%@@%#++++++++##===%##########
    ########=======#@@@%%@@%%@#+++++++##=====%##########
    ########@%%%%=#@@@@@@%%@@#+++=###%%%%%%%@###########
    ##############@@@%@%@%@@@################@@@########
    ########=@@@@@##%%%@%%@%#%%%%%%%%%%%%%%%%%=#########
    #########=%%%%%%%%###%%@#%%%%%%%%%%%%%%%%+##########
    #############+%%%%%%%%%%############################


Hello, this is a poor parody on TeeWorlds c:


REQUIREMENTS:

    1) this game uses official version of PYSFML, so
            http://www.python-sfml.org/

    2) also python3 is requred (I uses python33 as myself)

    2) do not use russian letters in full directory name, where game located :c


HOW TO PLAY!?
    

    1) teeworlds.py [-h] [-f] [-r RESOLUTION]

        THE GAME.

        -f - fullscreen mode
        -r RESOLUTION - resolution of game. see -h to take list of available resolutions.


        run this script and enjoy it c:


        ...after succesful connection to the server.

        Note1: 
            there are exist primitive shell
            use /help for command list
            use /help <command> or /<command> /h to show help about command
            use /help, bro c:

        Connectoin procces:
            1) type /help to see command list
            2) type /connect [ip=127.0.0.1] [port=65156]
                if you connect to localhost type just /connect
                if you connect to host with standart port type /connect ip.write.right.here
            3) notice thet client downdoad server level, enjoy it
            4) register on the server: type /register <YOUR_NICKNAME>
            5) check the welcome message from the server
            6) type /r for respawn
            7) KILL THEM ALL

        Note2:
            execute /admin PASSWORD command to get admin premissions
            it will gave you aility to use /set and /get commands
            see /help about it ;)

        Note3:
            there are exists some aliases
                /connect - c
                /help - /h
                /register - /reg
                /respawn - /r
                etc...
                see /help to full list. 

        Keys:
            w|a|s|d - control
            space - jump
            left_mouse - shoot
            rigth_mouse - hook
            1|2|3|4 - switch weapon

    2) server.py [-h] [-d]

        script to hosting the game. 
            automaticly binds on port 65156, if it is busy take the other one
            anyway shows it to you c:

        there is one useful argument [-d]
            it switch on the debug mode where draws for you what happends on server
            and prit extra log information


Some extra info:

    There are exist some additional modules:

        1) physic_light.py [levelname]
            It is light offline version of game, just for fun!

            Args:
                levelname - situatable parameter - load specific levelname from /src/levels/
                    by default it is 'house'

        2) map_editor.py [levelname]
            It is MAP_EDITOR beta version.
            You can drag and place tiles on level

            Keys:
                w|a|s|d - move camera
                +|- - movespeed camera
                ctrl+b - return camera to (0, 0) position
                ctrl+s - save progress (nothing happend, but trust me c:)
                1|2|3|4 - switch tile type Empty|Solid|Spawn|Thorns
                arrows - navigation on tileset
                mouse left click - pickup the tile, or place it if on map.

            Args:
                levelname - situatable parameter - load specific levelname from /src/levels/
                    by default it is 'house'

            Note:
                to create new level
                make new .tmf file in /src/levels/
                put inthere two strings:
                    +--------------------
                    |<file_name_without_.tmf>
                    |<img_source_name_from_/src/data/images/>
                    |~
                example:
                    +-----my_map.tmf------
                    |my_map
                    |jungle_main.png
                    |~
                and execute script like:
                    >map_esitor.py my_map

                img_source will be current map tileset! so choice wisely c:
