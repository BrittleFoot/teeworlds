#!/usr/bin/env python3
import SFML as sf
import argparse as arp
import logging as log
import sys

from SFML import CircleShape
from SFML import Clock
from SFML import Color
from SFML import Keyboard as key
from SFML import Mouse
from SFML import RectangleShape
from SFML import Vector2
from queue import Empty
from src.bind_system import check_binded
from src.client_draw_state import DrowDataState
from src.client_event_handler import ClientEventHandler
from src.client_invoker import send_command
from src.console_in_game import Console
from src.cursor_ext import update_cursor
from src.log_in_game import Log
from src.network.commands_codes import CODE
from src.network.network_client import Client
from src.network.network_proto import TIMEOUT
from src.smart_view import SmartView
from src.util.asynccall import call_async
from src.util.clay import Clay
from src.util.mouse import Cursor
loghead = '%(levelname)7s: %(module)s: '
logbody = '%(funcName)s %(lineno)3d> %(message)s'
log.basicConfig(level=log.DEBUG,
                format=loghead + logbody)


pressed = key.is_key_pressed


class Mainform():

    def __init__(self, vmode, FULLSCREEN):
        self.vmode = vmode
        style = [sf.Style.TITLEBAR, sf.Style.FULLSCREEN][FULLSCREEN]
        self.window = sf.RenderWindow(vmode, 'TeeWorlds', style)
        self.window.mouse_cursor_visible = False
        self.window.framerate_limit = 33
        self.view = SmartView(self.vmode, self.window)
        self.bg = sf.RectangleShape()
        self.bg.size = self.window.size
        self.bg.texture = sf.Texture.from_file('src/data/images/bg.jpg')
        self.bg.texture.smooth = True
        self.bg.fill_color = sf.Color(255, 255, 255, 150)

        self.client = Client()
        self.client.handle = self.reslove_server_answers

        self.console = Console(self.window, self.vmode, self.view)
        self.log = Log(self.window, 10, self.view, self.console)

        self.cursor = Cursor(self.window, self.vmode, self.view)
        self.view.mouse = self.cursor

        self.event_handler = ClientEventHandler(
            self.client.send,
            self.client.send_noc,
            self.cursor
        )

        self.state = DrowDataState(
            self.window,
            self.view,
            self.log,
            self.cursor
        )

        self.has_cursor = 1
        self.has_focus = 1
        self.in_console = 0

    def reslove_server_answers(self, command, args):
        self.state.resolve(command, args)

    def mainloop(self):

        self.client.bind()
        self.event_handler.start()
        self.state.update_timer.start()
        SPEED = 3
        clock = Clock()
        try:
            while self.window.is_open:
                time = clock.elapsed_time.microseconds
                clock.restart()

                for event in self.window.events:
                    self.check_system_events(event)
                    self.check_controll_events(event, time)

                if not (self.view.target or self.in_console):
                    if pressed(key.W):
                        self.view.center -= 0, SPEED * time / 1000
                    if pressed(key.S):
                        self.view.center += 0, SPEED * time / 1000
                    if pressed(key.A):
                        self.view.center -= SPEED * time / 1000, 0
                    if pressed(key.D):
                        self.view.center += SPEED * time / 1000, 0

                self.window.clear()
                self.bg.position = self.view.corner
                self.bg.ratio = self.view.ratio
                self.window.draw(self.bg)
                ######################################
                self.view.update(time)

                self.state.draw()

                ######################################
                if self.in_console:
                    self.console.update(time)
                    self.console.draw()
                self.log.update(time, self.in_console)
                self.log.draw()
                update_cursor(self.view, self.cursor)
                self.cursor.update(time)
                self.cursor.draw()
                ######################################
                self.window.display()
        except KeyboardInterrupt:
            log.error('Keyboard Interrupt!')
        finally:
            self.client.stop()
            self.event_handler.stop()
            self.state.update_timer.enabled = 0

    def check_system_events(self, event):

        if event == sf.Event.CLOSED:
            self.window.close()

        elif event == sf.Event.GAINED_FOCUS:
            self.has_focus = True
        elif event == sf.Event.LOST_FOCUS:
            self.has_focus = False

        elif event == sf.Event.MOUSE_ENTERED:
            self.has_cursor = True
        elif event == sf.Event.MOUSE_LEFT:
            self.has_cursor = False

        elif event == sf.Event.KEY_PRESSED and self.has_focus:
            if event['alt'] and event['code'] == key.F4:
                self.window.close()

    def check_controll_events(self, event, time):
        if not self.has_focus:
            return

        # -----------------------------------------------
        if event == sf.Event.KEY_PRESSED:
            binded_command = check_binded(event)
            if not self.in_console and binded_command:
                call_async(send_command,
                           self.client,
                           binded_command,
                           self.log.writeline)

            if self.in_console:
                self.console.check_key(event)
            else:
                if event['code'] == key.RETURN:
                    self.in_console = 1
                    call_async(self.get_command_from_console)

                self.event_handler.check_key(event, time, pressed=True)

        elif event == sf.Event.KEY_RELEASED:
            if not self.in_console:
                self.event_handler.check_key(event, time, pressed=False)

        # -----------------------------------------------

        elif event == sf.Event.TEXT_ENTERED and self.in_console:
            self.console.text_input(event)

        elif self.has_cursor and not self.in_console:
            handler = self.event_handler

            if event == sf.Event.MOUSE_MOVED:
                handler.check_mouse_move(event, time)
            elif event == sf.Event.MOUSE_BUTTON_PRESSED:
                handler.press_mouse_button(event['button'], time, True)
            elif event == sf.Event.MOUSE_BUTTON_RELEASED:
                handler.press_mouse_button(event['button'], time, False)

    def get_command_from_console(self):
        while self.window.is_open:
            try:
                command = self.console.readline(TIMEOUT)
                if command:
                    # Распознать комманду, пришедшую из консольки.
                    send_command(self.client, command, self.log.writeline)
                break
            except Empty:
                continue
        self.in_console = 0


def main():

    res = sf.VideoMode.get_fullscreen_modes()
    res = tuple(x for x in res if abs(x.width / x.height - 16 / 9) < 0.01)
    res_str = '\n'.join(('%2d.) %s' % (i, res[i]) for i in range(len(res))))

    def get_resolution(r):
        if r in range(len(res)):
            return res[r]
        else:
            return res[-1]

    fmt_class = arp.RawDescriptionHelpFormatter
    description = 'TeeWorlds client!\n\nResolutions:\n%s' % res_str
    parser = arp.ArgumentParser(description=description,
                                formatter_class=fmt_class)
    parser.add_argument('-r',
                        type=int,
                        default=2,
                        help='set resolution number')
    parser.add_argument('-f',
                        dest='set_fullscreen',
                        action='store_const',
                        const=True,
                        default=False,
                        help='fullscreen mode')

    args = parser.parse_args()

    form = Mainform(get_resolution(args.r), args.set_fullscreen)
    form.mainloop()


if __name__ == '__main__':
    main()
