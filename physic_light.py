import SFML as sf
import math as m
import src

from random import choice

from SFML import Vector2
from src.network.commands_codes import SPLITTER
from src.vector_extends import *

from src.level_tools import Level
from src.smart_view import SmartView
from src.tile_types import *
from src.util.mouse import Cursor

from src.network.commands_codes import CODE
from src.object_manager import ComparableObjectManager
from src.object_manager import ObjectManager
from src.physic_primtives import cfg
from src.physical_engine import PhysicalEngine
from src.util.events import mKeyEvent
from src.util.events import mMouseEvent

from src.ammo import Granader
from src.ammo import Gun
from src.ammo import Hammer
from src.ammo import Shotgun
from src.ammo import Weapon
from src.tee import Tee
from src.tee import weapon_number

from src.deserializer import deserialize
from src.serializer import serialize


def main():

    from sys import argv
    if len(argv) > 1:
        path = argv[1]
    else:
        path = 'house'

    level = Level.load_from_file(path)

    vmode = sf.VideoMode(1366, 768)
    style = sf.Style.FULLSCREEN
    style = sf.Style.TITLEBAR
    window = sf.RenderWindow(vmode, "Object interconnection example.", style)
    window.key_repeat_enabled = False
    window.framerate_limit = 60
    view = SmartView(vmode, window)
    view.view.size *= 1.3
    cursor = Cursor(window, vmode, view)
    view.mouse = cursor


    balls = ObjectManager(level)
    phe_e = PhysicalEngine(level, balls)

    # Доделать: Создание.

    ball = Tee()
    ball.name = 'b1'
    ball.position = choice(list(level.get_where(SPAWN))).position
    ball.fill_color = sf.Color.RED


    for x in range(20):
        some = Tee()
        some.name = str(x)
        some.position = choice(list(level.get_where(SPAWN))).position
        some.fill_color = sf.Color.CYAN
        some.F += 0, cfg['GRAVITY_FORCE']

        balls.add(some)

    balls.add(ball)

    view.target = ball

    ball.F += 0, cfg['GRAVITY_FORCE']

    clock = sf.Clock()

    balls.add_weapon(Hammer(ball.position - (0, 200)))
    balls.add_weapon(Gun(ball.position - (300, 200)))
    balls.add_weapon(Shotgun(ball.position - (600, 200)))
    balls.add_weapon(Granader(ball.position - (900, 200)))

    key = sf.Keyboard
    pressed = key.is_key_pressed

    shots = []


    while window.is_open:
        time = clock.elapsed_time.microseconds
        rds = cfg['GAME_SPEED_REDUCTION']
        maximum_delay = 20
        time = min(maximum_delay, time / rds)
        clock.restart()

        mpos = cursor.position

        for event in window.events:
            if event == sf.Event.CLOSED:
                window.close()
            if event == sf.Event.MOUSE_BUTTON_PRESSED or event == sf.Event.MOUSE_BUTTON_RELEASED:
                pressed = event == sf.Event.MOUSE_BUTTON_PRESSED
                if event['button'] == 1:
                    e = mMouseEvent(pressed, CODE.RMOUSE, mpos)
                    balls.add_event(ball.name, e)

                if event['button'] == 0:
                    e = mMouseEvent(pressed, CODE.LMOUSE, mpos)
                    balls.add_event(ball.name, e)

            if event == sf.Event.KEY_PRESSED or event == sf.Event.KEY_RELEASED:
                code = event['code']
                pressed = event == sf.Event.KEY_PRESSED

                if code == key.A:
                    e = mKeyEvent(pressed, CODE.LEFT)
                    balls.add_event(ball.name, e)
                if code == key.D:
                    e = mKeyEvent(pressed, CODE.RIGHT)
                    balls.add_event(ball.name, e)
                if code == key.SPACE:
                    e = mKeyEvent(pressed, CODE.JUMP)
                    balls.add_event(ball.name, e)
                if code in weapon_number:
                    e = mKeyEvent(pressed, code)
                    balls.add_event(ball.name, e)

        e = mMouseEvent(0, None, mpos)
        balls.add_event(ball.name, e)


        check_time = maximum_delay / 4
        while time > 0:
            t = time if time < check_time else check_time
            time -= check_time
            phe_e.check_coll(t)

            balls.update(t)

            view.update(t)
            cursor.update(t)

        balls.empty_trash()

        window.clear()
        level.draw(window)


        packed = balls.pack_to_only_drawable_format()


        shot = ComparableObjectManager(SPLITTER.join(packed))


        shots.append(shot)
        shots = shots[-10:]
        shot.draw(window)

        # for s in shots:
        #     s.draw(window)

        window.display()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        raise e
