#!/usr/bin/env python3
import argparse as arp
import logging as log

from SFML import Clock
from queue import Empty
from src.network.commands_codes import CMD
from src.network.commands_codes import CODE
from src.network.network_proto import TIMEOUT
from src.network.network_server import Server
from src.server_state import ServerGameState
from src.util.asynccall import call_async
from src.util.clay import Clay
from src.util.timer import Timer
from time import sleep
from time import sleep

import SFML as sf

UPDATE_FREQ = 1 / 99

DBG = 0


class GameServer:

    """ game controller & user server """

    def __init__(self):
        self.server = Server()
        self.server.handle = self.invoke
        self.server.start()
        self.state = ServerGameState(self.server)
        self.main_timer_enabled = 1

    def mainloop(self):
        self.main_timer = call_async(self.tick_tack, UPDATE_FREQ)
        self.gameloop()

    def stop(self):
        self.server.stop()
        self.state.share_timer.enabled = 0
        self.main_timer_enabled = 0

    def invoke(self, command, args, client_id):
        """ handler of all deamands """
        server = self.server
        if DBG:
            log.debug('{} sent {} : {}'.format(client_id, command, args))

        self.state.resolve_demand(command, args, client_id)

    def gameloop(self):
        clock = Clock()
        self.state.share_timer.start()

        while 1:
            sleep(1/20)

    def tick_tack(self, delay):
        """ gamestate update """
        clock = sf.Clock()

        while self.main_timer_enabled:
            try:
                time = clock.elapsed_time.microseconds
                clock.restart()
                self.state.dispatch_events()
                self.state.update(time)
            except Exception as e:
                log.error(e)

            sleep(delay)


def main():
    loghead = '%(levelname)7s: %(module)s: '
    logbody = '%(lineno)3d> %(message)s'
    log.basicConfig(level=log.DEBUG,
                    format=loghead + logbody)

    parser = arp.ArgumentParser()
    parser.add_argument('-d', '--dbg',
                        dest='debug_mode',
                        action='store_const',
                        const=1, default=0,
                        help='activate debug mode. !Hardly redusing perfomance!')

    args = parser.parse_args()
    global DBG
    DBG = args.debug_mode

    try:
        game = GameServer()
        game.mainloop()

    except KeyboardInterrupt:
        log.error('Keyboard Interrupt!')
    finally:
        game.stop()


if __name__ == '__main__':
    main()
