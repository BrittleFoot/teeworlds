#!/usr/bin/env python3
from os.path import join


class cfg():
    _cfg = None

    def __setitem__(self, key, value):
        cfg._cfg[key] = value

    def __getitem__(self, key):
        return cfg._cfg[key]

    def load_cfg():
        d = dict()
        print('cfg_loading...', end=' ')
        with open(join('src', 'physic.cfg')) as f:
            for line in f.read().split('\n'):
                if line:
                    var, val = line.split()
                    d[var] = float(val)
        cfg._cfg = d
        print('done.')

    def save_cfg(cfg):
        d = cfg._cfg
        for k, v in cfg:
            d[k] = v
        with open(CD + 'physic.cfg', mode='w') as f:
            for k, v in d.items():
                f.write('%s %s' % (k, v))


cfg.load_cfg()
