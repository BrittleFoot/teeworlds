#!/usr/bin/env python3
import SFML as sf

from src.tile_types import *
from src.tile_types import TILESIZE
from src.tileset import Tileset
from src.util.clay import Clay
from threading import Lock

from os.path import join
IMG_D = 'src/data/images/'
LEVELSDIR = 'src/levels/'

TYPE_COLOR = [sf.Color.WHITE,
              sf.Color.BLUE,
              sf.Color.RED,
              sf.Color.GREEN,
              sf.Color.YELLOW]


class Tile(object):

    """docstring for Tile
    tid == id on tileset 
    """
    LENGTH_SERIALIZED = 10

    def __init__(self, drawable, type, position, tid=0xff):
        self.drawable = drawable

        self.type = type
        self.position = position
        self.tid = tid

        self.rect = sf.RectangleShape((TILESIZE, TILESIZE))
        self.rect.position = self.position

        self.set_default_colorize()

    def set_default_colorize(self):
        self.rect.texture = None
        self.rect.fill_color = TYPE_COLOR[self.type]

    def set_texture(self, texture, texture_rectangle):
        self.rect.fill_color = sf.Color.WHITE
        self.rect.texture = texture
        self.rect.texture_rectangle = texture_rectangle

    @property
    def bounds(self):
        return self.rect.global_bounds

    def draw(self, window):
        if self.tid == 0xff:
            self.set_default_colorize()
        window.draw(self.rect)

    def __str__(self):
        c, r = self.position
        r, c = r // TILESIZE, c // TILESIZE
        return 'Tile(grid=%s)' % str((r, c))

    def __repr__(self):
        return self.__str__()

    def serialize(self):
        i_type = self.type.to_bytes(1, 'little', signed=1)
        if self.tid == -1:
            self.tid = 0xff
        i_tid = self.tid.to_bytes(1, 'little', signed=0)
        x, y = self.position
        x, y = int(x), int(y)
        x = x.to_bytes(4, 'little', signed=1)
        y = y.to_bytes(4, 'little', signed=1)
        return i_type + i_tid + x + y

    @classmethod
    def deserialize(cls, bytestr):
        d_type = int.from_bytes(bytestr[0:1], 'little', signed=1)
        d_tid = int.from_bytes(bytestr[1:2], 'little', signed=0)
        d_x = int.from_bytes(bytestr[2:6], 'little', signed=1)
        d_y = int.from_bytes(bytestr[6:10], 'little', signed=1)

        return cls(1, d_type, sf.Vector2(d_x, d_y), d_tid)


class Level(object):

    """ c_ == column, r_ == row """

    __THE_MAGIC_WORD = 'azaza'

    def __init__(self, token, tileset_name='grass_main.png'):
        if token is not self.__THE_MAGIC_WORD:
            raise ValueError("use construct functions")

        self.tile = {}  # dict[row, col] = tile
        self.tileset = Tileset(tileset_name)
        self.empty = False
        self.drawable = False or __name__ == '__main__'

        self.tile_lock = Lock()
        self.tileset_refreshed = 0

    def __iter__(self):
        for tile in self.tile.values():
            yield tile

    def get_where(self, type):
        yield from (tile for tile in self if tile.type == type)

    def __getitem__(self, screen_coords):
        x, y = screen_coords
        c, r = x // TILESIZE, y // TILESIZE
        return self.get(r, c)

    def __setitem__(self, screen_coords, tile):
        x, y = screen_coords
        c, r = x // TILESIZE, y // TILESIZE
        self.tile[r, c] = tile

    def get(self, r, c):
        if (r, c) in self.tile:
            return self.tile[r, c]

    def get_env(self, rect, type=-1):
        r_top, c_left = sf.Vector2(rect.top, rect.left) // TILESIZE
        r_bot, c_right = sf.Vector2(rect.bottom, rect.right) // TILESIZE

        r_top, r_bot, c_left, c_right = map(
            int, [r_top, r_bot, c_left, c_right])

        for r in range(r_top, r_bot + 1):
            for c in range(c_left, c_right + 1):
                tile = self.get(r, c)
                if tile:
                    if type == -1 or tile.type == type:
                        yield tile

    def add_tile(self, tile):
        x, y = tile.position
        c, r = x // TILESIZE, y // TILESIZE

        with self.tile_lock:
            self.tile[r, c] = tile

    @classmethod
    def load_example(cls):
        self = cls(cls.__THE_MAGIC_WORD)

        self.name = 'example'

        d = self.drawable
        with open('src/levels/m_ex.txt') as f:
            r = 0
            for line in f:
                c = 0
                for char in line.rstrip():
                    tile = Tile(d, int(char), sf.Vector2(c, r) * TILESIZE)
                    self.tile[r, c] = tile

                    c += 1
                r += 1

        return self

    @classmethod
    def load_from_file(cls, file_name):
        self = cls(cls.__THE_MAGIC_WORD)

        if not file_name.endswith('.tmf'):
            file_name += '.tmf'
        with open(LEVELSDIR + file_name, mode='rb') as f:
            self.name = f.readline().strip().decode()
            tileset_name = f.readline().strip().decode()
            self.tileset = Tileset(tileset_name)
            bts = f.read(Tile.LENGTH_SERIALIZED)
            while bts:
                tile = Tile.deserialize(bts)
                self[tile.position] = tile
                bts = f.read(Tile.LENGTH_SERIALIZED)

        return self

    @classmethod
    def empty(cls):
        self = cls(cls.__THE_MAGIC_WORD)
        self.empty = True

        return self

    def draw(self, window, use_tileset=True):
        with self.tile_lock:
            for tile in self:
                if use_tileset or self.tileset is None:
                    self.tileset.apply_texture(tile)
                else:
                    tile.set_default_colorize()
                tile.draw(window)

    def save(level):
        with open(LEVELSDIR + level.name + '.tmf', mode='wb') as f:
            f.write(level.name.encode() + b'\n')
            f.write(level.tileset.img_name.encode() + b'\n')
            for tile in level:
                f.write(tile.serialize())


def main():
    window = sf.RenderWindow(sf.VideoMode(1366, 768), "example")

    rect = sf.RectangleShape((TILESIZE, TILESIZE))
    rect.fill_color = sf.Color.RED

    level = Level.load_example()

    t = Tile.deserialize(Tile(1, 1, (1, 1), 1).serialize())

    level.add_tile(Tile(1, SOLID, sf.Vector2(1, 1) * TILESIZE))

    while window.is_open:
        for event in window.events:
            if type(event) is sf.CloseEvent:
                window.close()
            if type(event) is sf.MouseButtonEvent and event.pressed:
                print(list(level.get_env(rect.global_bounds)))

        window.clear()

        rect.position = sf.Mouse.get_position(window)

        level.draw(window)
        window.draw(rect)

        window.display()


if __name__ == '__main__':
    main()
