import logging as log
import src.game_session as gm

from SFML import Color
from SFML import RectangleShape
from SFML import Vector2
from queue import Empty
from queue import Queue
from random import choice
from src.commands import Func
from src.level_tools import Level
from src.level_tools import TILESIZE
from src.network.commands_codes import CODE
from src.network.commands_codes import EVENT_CODES
from src.network.commands_codes import KEYS
from src.network.commands_codes import MOUSEEVENT
from src.network.commands_codes import MOUSES
from src.network.commands_codes import SPLITTER
from src.network.commands_codes import WHEELS
from src.object_manager import ComparableObjectManager
from src.object_manager import ObjectManager
from src.phrases import get_dict
from src.physic_primtives import cfg
from src.physical_engine import PhysicalEngine
from src.tee import weapon_number
from src.tee_texture_manager import get_list as get_skin_list
from src.tile_types import *
from src.util.asynccall import call_async
from src.util.clay import Clay
from src.util.dict_pare import DictPare
from src.util.events import mKeyEvent
from src.util.events import mMouseEvent
from src.util.timer import Timer
from threading import Lock


MARK = 'Server: %s'
MAP_SEND_SPEED = 50  # Tiles in one packet
SNAPHOT_FREQ = 1 / 33

ANY = 0
REGISTERED = 1
ADMIN = 2

Func = {i.name: int(i.alevel) for i in set(Func.values())}

lang = 'eng'
PHRASE = get_dict(lang, 'server')

FUNC_CODES = {
    CODE.NAME: 'register',
    CODE.MESSAGE: 'send_all',
    CODE.ADMIN: 'admin',
    CODE.SET: 'set',
    CODE.GET: 'get',
    CODE.RESPAWN: 'respawn',
    CODE.SKIN: 'skin'
}


class ServerGameState(object):

    def __init__(self, server, level='dm0', lang='eng'):
        ServerGameState.server = server
        self.assoc = DictPare('cid', 'name')
        self.prem = dict()  # {cid: premition}

        self.level = Level.load_from_file(level)
        self.om = ObjectManager(self.level)
        self.pe = PhysicalEngine(self.level, self.om)

        self.share_timer = Timer(self.share, SNAPHOT_FREQ)

        self.event_lock = Lock()
        self.obj_lock = Lock()
        self.prem_lock = Lock()

    def check_prem(func_name, acces_level, cid):
        if func_name in Func and acces_level < Func[func_name]:
            msg = MARK % PHRASE['acces_den'].format(alvl=acces_level)
            ServerGameState.server.send_to(cid, CODE.ERROR, msg)
            return 0
        else:
            return 1

    def acceseble_function(func):
        def acceseble(*args):
            if ServerGameState.check_prem(func.__name__, *args[1:3]):
                return func(args[0], *args[2:])
        return acceseble

    def resolve_demand(self, command, args, cid):
        """ Обрабатывает асинхронно приходящие запросы. """
        if command == CODE.EVENTS:
            if self.prem[cid] > 0:
                self.resolve_events(args, cid)
        else:
            self.resolve_command(command, args, cid)

    def resolve_events(self, args, cid):
        for bevent in args.split(SPLITTER):
            etype = bevent[0:1]
            ekey = bevent[1:2]

            pressed = etype == CODE.PRESS

            event = None

            if ekey in KEYS:
                event = mKeyEvent(pressed, ekey)

            elif ekey in MOUSES or ekey == b'\x00':
                mcoords = Vector2(*MOUSEEVENT.unpack(bevent[2:10]))
                event = mMouseEvent(pressed, ekey, mcoords)

            elif ekey[0] in weapon_number:
                event = mKeyEvent(pressed, ekey[0])

            if event is not None:
                self.om.add_event(self.assoc[cid], event)

    def resolve_command(self, cmd, args, cid):
        global FUNC_CODES
        alevel = self.prem.get(cid, -1)
        func_args = (alevel, cid, args)

        if cmd == CODE.CONNECTED:
            with self.prem_lock:
                self.prem[cid] = ANY
            self.server.send_to(cid, CODE.INFO, MARK % PHRASE['welcome'])

            self.share_map_async(cid)

        elif cmd == CODE.DISCONNECTED:
            self.disconnect_user(cid)
        else:
            fname = FUNC_CODES.get(cmd, 'not_realized')
            try:
                func = getattr(self, fname)
            except AttributeError as e:
                SLASH = ['/', '\\'][getattr(__import__('os'), 'name') == 'nt']
                eargs = (fname, __file__.rsplit(SLASH, 1)[-1])
                msg = "Function '%s' not found in %s" % eargs
                log.error(msg)
                self.server.send_to(cid, CODE.ERROR, msg)

            try:
                func(*func_args)
            except Exception as e:
                log.error('%s' % e)
                self.server.send_to(cid, CODE.ERROR, str(e))

    def disconnect_user(self, cid):
        with self.prem_lock:
            popped_cid = self.prem.pop(cid)
        name = self.assoc.del_name(cid)

    @acceseble_function
    def not_realized(self, cid, *args):
        extra_info = 'invoked with args: %s' % args
        self.server.send_to(cid, CODE.ERROR, 'Not Realized, ' + extra_info)

    @acceseble_function
    def register(self, cid, args):
        name = args.decode()
        try:
            if len(name.encode()) > 20:
                msg = PHRASE['long_name'].format(name=name)
                self.server.send_to(cid, CODE.ERROR, MARK % msg)
                return 0

            self.assoc.add_name(cid, name)

            phrase = PHRASE['after_reg'].format(name=name)

            self.prem[cid] = REGISTERED

            self.server.send_to(cid, CODE.NAME, name)
            self.server.send_to(cid, CODE.INFO, MARK % phrase)

        except KeyError as e:
            self.server.send_to(cid, CODE.ERROR, MARK % str(e))
        else:
            self.server.send_to(cid, CODE.NAME, name)

    @acceseble_function
    def respawn(self, cid, args):
        spawned, answer = gm.spawn_tee(self.om, self.assoc[cid], args)

        if not spawned:
            answer = "Spawn response denied. Reason: %s" % answer
            self.server.send_to(cid, CODE.ERROR, answer)
        else:
            self.server.send_to(cid, CODE.NAME, answer)

    @acceseble_function
    def send_all(self, cid, args):
        msg = '%s: %s' % (self.assoc[cid], args.decode())
        self.server.send_to_anyone(CODE.MESSAGE, msg)

    @acceseble_function
    def admin(self, cid, args):
        self.server.send_to(cid, CODE.INFO, 'Hi, my new admin.')
        self.prem[cid] = ADMIN

    @acceseble_function
    def get(self, cid, args):
        var = args.decode().split()
        if len(var) == 0 or var[0] == 'ANY':
            var = cfg._cfg.keys()

        res = ''
        for name in var:
            val = str(cfg._cfg.get(name, ' \\(^_^)/'))
            res += '%s = %s\n' % (name, val)

        self.server.send_to(cid, CODE.INFO, res)

    @acceseble_function
    def set(self, cid, args):
        equality = (pare.split('=') for pare in args.decode().split())

        for var, value in equality:
            value = float(value)
            cfg._cfg[var] = value

        self.server.send_to(cid, CODE.INFO, "Done!")

    @acceseble_function
    def skin(self, cid, args):
        if not args:
            info = 'List of available skins:\n\t'
            info += '\n\t'.join(get_skin_list())

            self.server.send_to(cid, CODE.INFO, info)
        else:
            name = args.decode().lower()

            if name not in get_skin_list():
                emsg = "Skin %s does not exist ;c"
                self.server.send_to(cid, CODE.ERROR, emsg)
                return

            self.om.objects[self.assoc[cid]].skin = name

    def dispatch_events(self):
        """ 
        Синхронизирует асинхронную обратотку запросов,
        информирующих о произошедших эвентах со стороны игроков.
        """
        pass

    def update(self, time):
        """ Тик игры. """
        rds = cfg['GAME_SPEED_REDUCTION']
        maximum_delay = 20
        time = min(maximum_delay, time / rds)

        check_time = maximum_delay / 4
        while time > 0:
            t = time if time < check_time else check_time
            time -= check_time
            self.pe.check_coll(t)
            self.om.update(t)

        self.om.empty_trash()

    def draw(self, window):

        self.level.draw(window)
        self.om.draw(window)

    def share(self, time):
        """ Раздает всем игровые объекты. """
        packed = SPLITTER.join(self.om.pack_to_only_drawable_format())
        controll_length = len(packed)
        if controll_length:

            self.server.send_all_noblock(CODE.OBJ_HEADER)

            while packed:
                part = packed[:1000]
                packed = packed[1000:]

                self.server.send_all_noblock(CODE.OBJ_DATA, part)

            self.server.send_all_noblock(
                CODE.OBJ_DATA_END, str(controll_length))

    def share_map_async(self, cid):
        call_async(self.__share_map, cid)

    def __share_map(self, cid):

        def send(cmd, args=None):
            self.server.send_to(cid, cmd, args)

        HEAD = CODE.MAP_HEADER
        DATE = CODE.MAP_DATE
        END = CODE.MAP_DATE_END

        head = '%s|%s' % (self.level.name, self.level.tileset.img_name)

        send(HEAD, head)

        i = 0
        pocket = b''
        for tile in self.level:
            pocket += tile.serialize()
            i += 1

            if i > MAP_SEND_SPEED:
                send(DATE, pocket)
                pocket = b''
                i = 0

        if pocket:
            send(DATE, pocket)
            pocket = b''

        send(END)
