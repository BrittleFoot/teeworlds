#!/usr/bin/env python3
from os.path import join
with open(join('src', 'phrases', 'separator.cfg')) as file:
    __SEP = ' %s ' % file.readline().rstrip()


class Phraser(object):

    """docstring for Phraser"""

    def __init__(self, dic):
        self._dic = dic

    def __getitem__(self, key):
        try:
            return self._dic[key]
        except KeyError:
            return key


def get_dict(lang, target):
    d = {}
    filename = join('src', 'phrases', lang, target + '.dic')
    try:
        with open(filename) as file:
            for line in file.read().rstrip().split('\n'):
                key, value = line.split(__SEP, 1)
                d[key] = value
    except ValueError:
        raise OSError('File %s is corrupted.' % filename)
    else:
        return Phraser(d)
