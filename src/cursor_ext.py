from SFML import Rectangle
from SFML import Texture
from SFML import Vector2
from src.ammo import WIMG
from src.data import get_img

CIMG = Texture.from_file(get_img('cursor.png'))

HAMMER_RECT = Rectangle((0, 0), (64, 64))
GUN_RECT = Rectangle((0, 128), (64, 64))
SHOTGUN_RECT = Rectangle((0, 192), (64, 64))
GRANADER_RECT = Rectangle((0, 256), (64, 64))


def update_cursor(mouse_handler, cursor_handler):
    mh = mouse_handler
    cursor = cursor_handler.cursor

    if not mh.target:
        cursor.origin = Vector2(0, 0)
        cursor.texture = CIMG
        return

    if hasattr(mh.target, 'curr_weap'):
        cursor.origin = Vector2(cursor_handler.radius, cursor_handler.radius)
        cursor.texture = WIMG
        weap_rect_name = mh.target.curr_weap.upper() + "_RECT"
        cursor.texture_rectangle = globals().get(weap_rect_name)
