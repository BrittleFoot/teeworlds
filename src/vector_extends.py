#!/usr/bin/env python3
import math as m

from SFML import Vector2

EPS = 1e-9


def orient(a, b, c):
    return (a.x - c.x) * (b.y - c.y) - (a.y - c.y) * (b.x - c.x)


def rotate(vector, radian):
    x, y = vector
    sin, cos = m.sin(radian), m.cos(radian)
    xx = x * cos - y * sin
    yy = x * sin + y * cos
    return Vector2(xx, yy)


def rotate_d(vector, degrees):
    return rotate(vector, to_radian(degrees))


def angle(vector):
    return m.atan2(vector.y, vector.x)


def angle_d(vector):
    return to_degrees(angle(vector))


def l(vector):
    x, y = vector
    return m.sqrt(x * x + y * y)


def cos_btw(v1, v2):
    return (v1.x * v2.x + v1.y * v2.y) / l(v1) / l(v2)


def n(vector):
    length = l(vector)
    if length > EPS:
        return vector / l(vector)
    else:
        return vector


def to_degrees(radian):
    return radian * 180 / m.pi


def to_radian(degrees):
    return degrees * m.pi / 180


def main():
    pass


if __name__ == '__main__':
    main()
