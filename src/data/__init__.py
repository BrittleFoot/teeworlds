#!/usr/bin/env python3
from os.path import join


def get_img(by_name):
    return join('src', 'data', 'images', by_name)
