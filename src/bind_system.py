import logging as log

from SFML import Keyboard


_valid_keys = [field for field in dir(Keyboard) if not field.startswith('__')]
_valid_keys.remove('is_key_pressed')
_valid_keys.remove('KEY_COUNT')
_valid_keys = set(_valid_keys)

__error = 0


def check_binded(kevent):
    key_extracted = kevent['code'], kevent['control'], kevent['alt'], kevent['shift']
    return _associations.get(key_extracted, None)


def _analys_line(line):
    """ Returns tuple (key, value) where:
        key is tuple looks like: (key, ctrl, alt, shift)
        value is associtated for thoose combination string
    """
    keys, command = line.split(' ', maxsplit=1)

    keys = keys.upper().split('+')

    mods = {'CTRL': 0, 'ALT': 0, 'SHIFT': 0}

    for mod in mods:
        if keys.count(mod) == 1:
            keys.remove(mod)
            mods[mod] = 1
        if keys.count(mod) > 1:
            raise ValueError('I don\'t understand: %s' % line)

    if len(keys) == 1:
        key = keys[0]
        if key in _valid_keys:
            key = getattr(Keyboard, key)

            return (key, mods['CTRL'], mods['ALT'], mods['SHIFT']), command

        else:
            raise ValueError('I don\'t understand: %s' % line)

    else:
        raise ValueError('I don\'t understand: %s' % line)


_associations = dict()


with open('src/bind.cfg') as f:
    for line in f.read().strip().split('\n'):
        line = line.split('#')[0]
        if line:
            try:
                keys, command = _analys_line(line)
                _associations[keys] = command
            except Exception as e:
                log.error(e)
                __error = 1


if __error:
    log.info("File 'binf.cfg' partially corrupted, please check it c:")
