import math

from SFML import Color
from SFML import Keyboard as k
from SFML import Rectangle
from SFML import RectangleShape
from SFML import Texture
from SFML import Vector2
from os import walk
from src.vector_extends import angle_d
from src.vector_extends import l
from src.vector_extends import n
from src.vector_extends import rotate_d


IMG_PATH = 'src/data/images/skins/'

BIG_SIZE = Vector2(96, 96)
LITTLE_SIZE = Vector2(32, 32)
BROAD_SIZE = Vector2(64, 32)

r_BODY_FG = Rectangle((0, 0), BIG_SIZE)
r_BODY_BG = Rectangle((96, 0), BIG_SIZE)
r_ARM_FG = Rectangle((192, 0), LITTLE_SIZE)
r_ARM_BG = Rectangle((224, 0), LITTLE_SIZE)
r_LEG_FG = Rectangle((192, 32), BROAD_SIZE)
r_LEG_BG = Rectangle((192, 64), BROAD_SIZE)
r_EYE1 = Rectangle((64, 96), LITTLE_SIZE)
r_EYE2 = Rectangle((96, 96), LITTLE_SIZE)
r_EYE3 = Rectangle((128, 96), LITTLE_SIZE)
r_EYE4 = Rectangle((160, 96), LITTLE_SIZE)
r_EYE5 = Rectangle((192, 96), LITTLE_SIZE)
r_EYE6 = Rectangle((224, 96), LITTLE_SIZE)


class Skin(object):

    """docstring for Skin"""

    def __init__(self, name, texture):
        self.name = name
        self.texture = texture
        size = Vector2(132, 132)

        self.body_fg = RectangleShape(size)
        self.body_fg.texture = texture
        self.body_fg.texture_rectangle = r_BODY_FG
        self.body_bg = RectangleShape(size)
        self.body_bg.texture = texture
        self.body_bg.texture_rectangle = r_BODY_BG
        self.arm_fg = RectangleShape(r_ARM_FG.size * 1.5)
        self.arm_fg.texture = texture
        self.arm_fg.texture_rectangle = r_ARM_FG
        self.arm_bg = RectangleShape(r_ARM_BG.size * 1.5)
        self.arm_bg.texture = texture
        self.arm_bg.texture_rectangle = r_ARM_BG
        self.leg_fg = RectangleShape(r_LEG_FG.size * 1.5)
        self.leg_fg.texture = texture
        self.leg_fg.texture_rectangle = r_LEG_FG
        self.leg_bg = RectangleShape(r_LEG_BG.size * 1.5)
        self.leg_bg.texture = texture
        self.leg_bg.texture_rectangle = r_LEG_BG
        self.eye1 = RectangleShape(r_EYE1.size * 1.4)
        self.eye1.texture = texture
        self.eye1.texture_rectangle = r_EYE1
        self.eye2 = RectangleShape(r_EYE2.size * 1.4)
        self.eye2.texture = texture
        self.eye2.texture_rectangle = r_EYE2
        self.eye3 = RectangleShape(r_EYE3.size * 1.4)
        self.eye3.texture = texture
        self.eye3.texture_rectangle = r_EYE3
        self.eye4 = RectangleShape(r_EYE4.size * 1.4)
        self.eye4.texture = texture
        self.eye4.texture_rectangle = r_EYE4
        self.eye5 = RectangleShape(r_EYE5.size * 1.4)
        self.eye5.texture = texture
        self.eye5.texture_rectangle = r_EYE5
        self.eye6 = RectangleShape(r_EYE6.size * 1.4)
        self.eye6.texture = texture
        self.eye6.texture_rectangle = r_EYE6

        self.full = Color.WHITE
        self.gray = Color(150, 150, 150)

        self.arg = 0

    def rot_func(self, arg):
        """ leg rotation on move """
        return 6 * math.sin(arg) - 4

    def choose_eyes(self, tee):
        e1 = self.eye1
        e2 = self.eye1

        if tee.health < 3:
            e2 = self.eye2

        if tee.hitted:
            e1 = self.eye3

        if tee.health < 1:
            e1 = e2 = self.eye6

        return e1, e2

    def draw(self, window, tee):
        size_mod = 1.6
        corner = tee.position - tee.origin * size_mod

        self.body_bg.position = corner
        self.body_fg.position = corner

        p = k.is_key_pressed

        self.leg_fg.fill_color = [self.gray, self.full][bool(tee.can_jump)]
        self.arg = (self.arg + tee.speed.x / 5) % (2 * math.pi)
        rot = self.rot_func(self.arg)
        self.leg_fg.position = corner + (18, 72)
        self.leg_bg.position = corner + (18, 72)
        self.leg_fg.rotation = [0, rot][bool(tee.on_ground)]
        self.leg_bg.rotation = [0, rot][bool(tee.on_ground)]

        eye1, eye2 = self.choose_eyes(tee)
        eye1.position = corner + (32, 32 + 5)

        ofst = tee.mpos
        if (l(ofst) > 10):
            ofst = n(ofst) * 10
        eye1.position += ofst

        window.draw(self.body_bg)
        window.draw(self.body_fg)
        window.draw(self.leg_bg)
        window.draw(self.leg_fg)

        window.draw(eye1)
        eye2.position = eye1.position + (26, 0)
        window.draw(eye2)


class __TextureGod(object):

    """docstring for __TextureGod"""

    def __init__(self):
        directory = next(walk(IMG_PATH))
        files = directory[-1]
        self.textures = {}
        for file in files:
            name, type = file.rsplit('.', maxsplit=1)
            if type != 'png':
                continue
            try:
                texture = Texture.from_file(IMG_PATH + file)
                texture.smooth = True
                self.textures[name] = texture
            except Exception as e:
                print(e)
                continue

        self.skins = {}

        for name, texture in self.textures.items():
            self.skins[name] = Skin(name, texture)


__god = __TextureGod()


def get_list():
    return __god.skins.keys()


def draw(window, tee):
    global __god
    god = __god
    god.skins.get(tee.skin, god.skins['default']).draw(window, tee)
