from SFML import Vector2


def _is_int(x):
    try:
        int(x)
    except ValueError:
        return False
    else:
        return True


class FlexableEntity:

    def __init__(self, dictionary):
        """ Beware, that __dict is not cloneing, just ref copy,
            givn arrays will not being copied.
            so do not never ever use dictionary parameter, or fix it c:
        """
        self.__dict = dictionary
        for key, value in dictionary.items():

            if isinstance(value, dict):
                setattr(self, key, FlexableEntity(value))

            elif type(value) is list:
                if len(value) == 2 and _is_int(value[0]):
                    setattr(self, key, Vector2(*value))
                else:
                    setattr(self, key, value)

            else:
                setattr(self, key, value)

    def __str__(self):
        """ just_for_debug, rem about Vector2parsing """
        return str(self.__dict).replace('{', '(flexable){')

def init_from_dict(obj, dictionary):
    for key, value in dictionary.items():

        if isinstance(value, dict):
            raise NotImplemented(value + ' ' + str(dict))

        elif type(value) is list:
            if len(value) == 2 and _is_int(value[0]):
                setattr(obj, key, Vector2(*value))
            else:
                setattr(obj, key, value)

        else:
            setattr(obj, key, value)


