#!/usr/bin/env python3
import SFML as sf

from SFML import Vector2
from math import sqrt
from src.physic_cfg import cfg
from src.tile_types import *
from src.vector_extends import *
cfg = cfg()


HORIZONTAL = 0
VERTICAL = 1


__ID = 10


def get_id():
    global __ID
    __ID += 1
    return __ID


class Collision:

    def intersects(rect1, rect2):
        xcol = rect1.right > rect2.left and rect1.left < rect2.right
        ycol = rect1.bottom > rect2.top and rect1.top < rect2.bottom
        return xcol and ycol

    def balls_intersects(b1, b2):
        return l(b1.position - b2.position) <= b1.radius + b2.radius

    def with_levelitems(obj, level, direct):
        obj.friction = cfg['F_AIR']
        if level is not None:
            for tile in level.get_env(obj):
                rect = tile.bounds
                if Collision.intersects(obj, rect):
                    obj.solve_collision(rect, tile.type, direct)

    def with_rect_like_solid_obj(obj, rect, direct):
        if obj.V.y > 0 and direct == VERTICAL:
            obj.position = Vector2(obj.position.x, rect.top - obj.radius)
            obj.V = Vector2(obj.V.x, 0)
            obj.friction = cfg['F_GROUND']
            obj.on_ground = 3

        if obj.V.y < 0 and direct == VERTICAL:
            obj.position = Vector2(obj.position.x, rect.bottom + obj.radius)
            obj.V = Vector2(obj.V.x, 0)

        if obj.V.x > 0 and direct == HORIZONTAL:
            obj.position = Vector2(rect.left - obj.radius, obj.position.y)

        if obj.V.x < 0 and direct == HORIZONTAL:
            obj.position = Vector2(rect.right + obj.radius, obj.position.y)

    def ball_with_line(ball, x0, n):
        """x0 - point, n - vector-pointer"""
        c = l(n)
        a = l(ball.position - x0)
        b = l(x0 + n - ball.position)

        try:
            cosB = (a * a - c * c - b * b) / (-2 * c * b)
            cosY = (b * b - a * a - c * c) / (-2 * c * a)
        except ZeroDivisionError:
            return False

        if cosB < 0 or cosY < 0:
            return False

        sinB = sqrt(1 - cosB * cosB)
        h = b * sinB
        return h < ball.radius


class Influence(object):

    """docstring for Influence"""

    def __init__(self, target):
        self._map = dict()  # Object:int_influence_power
        self.target = target

    def add(self, obj, i_power):
        if isinstance(obj, Vector2):
            self._map[obj.x, obj.y] = i_power
        else:
            self._map[obj] = i_power

    def remove(self, obj):
        if isinstance(obj, Vector2):
            obj = obj.x, obj.y
        self._map.pop(obj, 0)

    def get(self):
        res = Vector2()
        for obj, power in self._map.items():
            if isinstance(obj, tuple):
                targ = Vector2(*obj)
            else:
                targ = obj.position
            dist = (targ - self.target.position)
            pew = sqrt(l(dist))
            res += n(dist) * power * pew
        return res


class Hook(object):

    """docstring for Hook"""
    NO_TARG_LIFE = 5
    CHECK_PER_TICK = 2

    def __init__(self, obj, dir_vect):
        self.obj = obj
        self.dir = n(dir_vect)

        self.rect = sf.RectangleShape()
        self.rect.position = self.obj.position
        self.rect.origin = (0, 3)
        self.rect.size = (0, 5)
        self.rect.rotation = angle_d(self.dir)
        self.normal = self.dir * self.rect.size.x
        self.norm = self.dir
        self.counter = 0
        self.target = None

    def __del__(self):

        if self.target:
            self.obj.influence.remove(self.target)
            if not isinstance(self.target, Vector2):
                self.target.influence.remove(self.obj)

    def tick(self, obj_list, level, mpressed):
        self.counter += 1

        if self.target is None:
            delta = cfg['HOOK_LENGTH'] / self.NO_TARG_LIFE

            target_tile, target_obj = None, None

            for i in range(1, self.CHECK_PER_TICK + 1):
                sz = delta * (self.counter - 1 + i / self.CHECK_PER_TICK)
                self.rect.size = sz, 5
                self.normal = self.dir * self.rect.size.x
                point = self.rect.position + self.normal

                tile = level[point]
                if tile is not None and tile.type == SOLID:
                    target_tile = point
                    break

            for obj in obj_list.get_others(self.obj):
                if Collision.ball_with_line(obj, self.rect.position, self.normal):
                    target_obj = obj

            if target_tile is not None and target_obj is not None:
                t_len = l(target_tile - self.rect.position)
                o_len = l(target_obj.position - self.rect.position)
                self.target = [target_obj, target_tile][t_len < o_len]
            elif target_tile is not None:
                self.target = target_tile
            elif target_obj is not None:
                self.target = target_obj
        else:
            if isinstance(self.target, Vector2):
                targ = self.target
            else:
                targ = self.target.position
            dist = targ - self.obj.position
            self.rect.position = self.obj.position
            self.rect.rotation = angle_d(dist)
            self.rect.size = l(dist), 5

            self.normal = dist

            self.obj.influence.add(self.target, cfg['HOOK_POWER'] * 0.7)
            if not isinstance(self.target, Vector2):
                self.target.influence.add(self.obj, cfg['HOOK_POWER'])

        cond = self.target is not None and mpressed
        SUP = [self.NO_TARG_LIFE, cfg['HOOK_SUSTAIN'], 10e100]
        cond2 = isinstance(self.target, Vector2) and mpressed
        if cond2:
            cond = 2

        return self.counter > SUP[cond]

    def draw(self, window):
        window.draw(self.rect)


class Object(sf.CircleShape):

    """Base physical active class.

    Represent ball-like objects.
    """
    level = None

    def __init__(self, radius):
        super(Object, self).__init__(radius)
        self.name = 'abstract'
        self.id = get_id()
        self.radius = radius
        self.origin += Vector2(self.radius, self.radius)

        self.type = self.__class__.__name__

        self.V = Vector2()
        self.M = m.pi * self.radius * self.radius * cfg['OBJ_DENSITY']
        self.F = Vector2()

        self.influence = Influence(self)

        self.friction = cfg['F_GROUND']
        self.on_ground = 3
        
    def __contains__(self, point):
        return l(point - self.position < self.radius)

    def contain(self, other):
        distance = other.position - self.position
        rr = (other.radius + self.radius)
        maxdist = n(distance) * rr

        diff = maxdist - distance
        return (False, diff)[l(distance) < rr]

    def update_position(self, dt):

        level = self.level

        self.position += 0, self.V.y * dt
        cy = Collision.with_levelitems(self, level, VERTICAL)

        self.position += self.V.x * dt, 0
        cx = Collision.with_levelitems(self, level, HORIZONTAL)

    def update_speed(self, dt):

        influence = self.influence.get()

        self.V += (self.F + influence) / self.M * dt
        vx, vy = self.V
        fric = self.friction
        if self.on_ground:
            fric = cfg['F_GROUND']
        self.V = Vector2(vx * fric, vy)

        if l(self.V) > 20:
            self.V = n(self.V) * 20

    def update(self, level, dt=1):
        self.level = level
        self.update_position(dt)
        self.update_speed(dt)
        if self.on_ground:
            self.on_ground -= 1

    def apply_impulse(self, normal, impulse):
        x, y = self.V
        fric = self.friction
        if self.on_ground:
            fric = cfg['F_GROUND']
        x += normal.x * (1 / self.M) * impulse * (1 - fric ** 3)
        y += normal.y * (1 / self.M) * impulse

        self.V = Vector2(x, y)

    def check_coll(self, other):
        intersect = -self.contain(other)
        if intersect:
            ilen = l(intersect)
            half_rad = self.radius / 2
            if ilen > half_rad:
                d = n(intersect) * (ilen - half_rad)
                intersect = n(intersect) * half_rad

                self.position += 0, d.y
                cx = Collision.with_levelitems(self, self.level, VERTICAL)

                self.position += d.x, 0
                cx = Collision.with_levelitems(self, self.level, HORIZONTAL)

            self.V += intersect * l(intersect) / 1000

    def solve_collision(self, tile_rect, tile_type, direct):
        """ ФУНКЦИЯ ДЛЯ ПЕРЕОПРЕДЕЛЕНИЯ В КЛАССАХ НАСЛЕДНИКАХ
        """
        if tile_type == SOLID:
            Collision.with_rect_like_solid_obj(self, tile_rect, direct)

    def draw(self, window):
        window.draw(self)

    @property
    def left(self):
        return self.position.x - self.radius

    @property
    def right(self):
        return self.position.x + self.radius

    @property
    def top(self):
        return self.position.y - self.radius

    @property
    def bottom(self):
        return self.position.y + self.radius

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return hash(self) == hash(other)
