#!/usr/bin/env python3
from SFML import CircleShape
from SFML import Rectangle
from SFML import RectangleShape
from SFML import Texture
from SFML import Vector2
from math import atan2
from math import pi
from math import sin
from src.data import get_img
from src.physic_primtives import Object
from src.physic_primtives import cfg
from src.tile_types import *
from src.vector_extends import angle_d
from src.vector_extends import l
from src.vector_extends import n
from src.vector_extends import rotate_d
from struct import Struct


WIMG = Texture.from_file(get_img('game.png'))
WIMG.smooth = True
R_HAMMER = Rectangle((64, 32), (64 * 2, 32 * 3))

R_GUN = Rectangle((64, 32 * 4), (32 * 4, 64))
B_GUN = Rectangle((R_GUN.right, R_GUN.top), (64, 64))

R_SHOTGUN = Rectangle((64, 64 * 3), (64 * 4, 64))
B_SHOTGUN = Rectangle((R_SHOTGUN.right, R_SHOTGUN.top), (64, 64))

R_GRANADER = Rectangle((64, 256), (64 * 4, 64))
B_GRANADER = Rectangle((R_GRANADER.right, R_GRANADER.top), (64, 64))


HAMMER = 0
GUN = 1
SGOTGUN = 2
GRANADER = 4

__WEAP_ID = 0


def get_id():
    global __WEAP_ID
    __WEAP_ID -= 1
    return __WEAP_ID


class Weapon(RectangleShape):

    """Abstract Weapon"""

    def __init__(self, position):
        super(Weapon, self).__init__(position)
        self.ammo = cfg['WEAPON_AMMO']
        self.id = get_id()
        self.position = position
        self.size = (48 * 4, 48)
        self.host = None
        self.name = self.__class__.__name__
        self.texture_rot = 0
        self.shoot_callback = 0
        self.origin = (self.size.x / 6, self.size.y / 2)
        self.shoot_dir = None
        self.timer = 0
        self.TIMEOUT = 100

    def shoot(self, direction, om=None, dt=None):
        if not self.shoot_callback or om is None:
            self.shoot_callback = 1
            self.shoot_dir = direction
        else:
            self.shoot_callback = 0
            self.shoot_dir = None

            if self.timer > self.TIMEOUT and self.ammo > 0:
                self.timer = 0
                self.ammo -= 1
                self._shoot(direction, om, dt)

    def _shoot(self, direction, om, dt):
        bullet = Bullet()
        bullet.host = self.host
        bullet.position = self.host.position + n(direction) * 10
        om.add_bullet(bullet.set_fire(direction))

    def reload(self):
        self.ammo = cfg['WEAPON_AMMO']

    def update(self, level, om, dt):
        self.timer += dt
        if level == "just_update_me":
            return "ok :("

        if self.shoot_callback:
            self.shoot(self.shoot_dir, om, dt)
        if self.host == 'dead_man':
            om.bury(self.id)
        elif self.host is not None:
            self.position = self.host.position
            mpos = self.host.em.mpos
            angle = atan2(mpos.y, mpos.x) * 180 / pi
            if angle > 90 or angle < -90:
                self.texture_rot = 1
            else:
                self.texture_rot = 0
            self.rotation = angle
            return 1
        return 0

    def draw(self, window):
        if self.host is None or self.host.curr_weap == self.name:
            window.draw(self)

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        is_subclass = issubclass(other, self)
        equal_id = getattr(other, 'id', 1) == self.id
        return is_subclass and equal_id


class Hammer(Weapon):

    """Hammer"""

    def __init__(self, position):
        super().__init__(position)
        self.ammo = 1
        self.name = 'Hammer'
        self.texture = WIMG
        self.texture_rectangle = R_HAMMER
        self.texture_rot = -1
        self.size = self.texture_rectangle.size * 1.2
        self.origin = self.size.x * 0.2, self.size.y * 0.5
        self.TIMEOUT = 25

    def _shoot(self, direction, om, dt):
        self.ammo = 1
        point = self.host.position + n(direction) * self.size.x * 3 / 4
        for obj in om.get_others(self.host):
            if l(obj.position - point) < obj.radius:
                d = n(direction) + (0, -0.3)
                obj.apply_impulse(d, cfg['HAMMER_POWER'] * dt)
                obj.hit(cfg['HAMMER_ATK'], self.host)

    def update(self, level, om, dt):
        if super().update(level, om, dt):
            self.rotation = {-1: 0, 0: -135, 1: -45}[self.texture_rot]


class Gun(Weapon):

    """Gun"""

    def __init__(self, position):
        super().__init__(position)
        self.texture = WIMG
        self.texture_rectangle = R_GUN
        self.size = self.texture_rectangle.size
        self.origin = (-self.size.x * 0.1, self.size.y * 0.5)
        self.TIMEOUT = 50

    def _shoot(self, direction, om, dt):
        bullet = GunBullet()
        bullet.host = self.host
        bullet.position = self.host.position - \
            self.origin / 2 + n(direction) * 50
        om.add_bullet(bullet.set_fire(direction))

    def update(self, level, om, dt):
        if super().update(level, om, dt):
            x, y = R_GUN.position
            w, h = R_GUN.size
            if self.texture_rot:
                self.texture_rectangle = Rectangle((x, y + h), (w, -h))
            else:
                self.texture_rectangle = R_GUN


class Shotgun(Weapon):

    """Shotgun"""

    def __init__(self, position):
        super().__init__(position)
        self.texture = WIMG
        self.texture_rectangle = R_SHOTGUN
        self.size = self.texture_rectangle.size * 3 / 4

    def _shoot(self, direction, om, dt):
        scatter = cfg['SHOTGUN_SCATTER'] / 2
        count = cfg['SHOTGUN_BULLETS']
        angle = angle_d(direction)
        angle -= scatter

        for i in range(int(count)):
            a = angle + 2 * i * scatter / count

            bullet = ShotgunBullet()
            bullet.host = self.host
            bullet.position = self.host.position + n(direction) * 10
            om.add_bullet(bullet.set_fire(rotate_d(Vector2(1, 0), a)))

    def update(self, level, om, dt):
        if super().update(level, om, dt):
            x, y = R_SHOTGUN.position
            w, h = R_SHOTGUN.size
            if self.texture_rot:
                self.texture_rectangle = Rectangle((x, y + h), (w, -h))
            else:
                self.texture_rectangle = R_SHOTGUN


class Granader(Weapon):

    """Granader"""

    def __init__(self, position):
        super().__init__(position)
        self.texture = WIMG
        self.texture_rectangle = R_GRANADER
        self.size = self.texture_rectangle.size * 3 / 4

    def _shoot(self, direction, om, dt):
        bullet = GranaderBullet()
        bullet.host = self.host
        bullet.position = self.host.position + n(direction) * 10
        om.add_bullet(bullet.set_fire(direction))

    def update(self, level, om, dt):
        if super().update(level, om, dt):
            x, y = R_GRANADER.position
            w, h = R_GRANADER.size
            if self.texture_rot:
                self.texture_rectangle = Rectangle((x, y + h), (w, -h))
            else:
                self.texture_rectangle = R_GRANADER


class Bullet(CircleShape):

    """Abstract Bullet"""

    def __init__(self, radius=10):
        super(Bullet, self).__init__(radius)
        self.id = get_id()
        self.origin = self.radius, self.radius

        # Mass, Force, Velocity
        self.M = pi * self.radius * self.radius * cfg['OBJ_DENSITY'] * 10
        self.F = Vector2(0, cfg['GRAVITY_FORCE'] / 2)
        self.V = Vector2(0, 0)
        self.TTL = 256  # Time to live

    def set_fire(self, father, direction):
        self.host = father
        self.V = n(direction) * 7
        return self

    def explode(self, om, reason):
        print('BOOM %s' % reason)
        om.bury(self.id)

    def update(self, level, om, dt):
        if level == "just_update_me":
            return "ok :("

        self.TTL -= 1
        self.position += self.V * dt
        self.V += self.F / self.M * dt

        tile = level[self.position]
        if tile is not None and tile.type == SOLID:
            self.explode(om, tile)

        if not self.TTL:
            self.explode(om, None)

    def draw(self, window):
        window.draw(self)


class GunBullet(Bullet):

    """GunBullet"""

    def __init__(self):
        super().__init__(30)
        self.texture = WIMG
        self.texture_rectangle = B_GUN

    def update(self, level, om, dt):
        super().update(level, om, dt)
        if l(self.V) > 0:
            self.rotation = angle_d(self.V)

    def set_fire(self, direction):
        self.V = n(direction) * cfg['GUN_SPEED']
        return self

    def explode(self, om, reason):
        if reason is not None and issubclass(type(reason), Object):
            if (getattr(type(reason), 'hit', False)):
                reason.hit(cfg['GUN_ATK'], self.host)
        om.bury(self.id)


class ShotgunBullet(Bullet):

    """ShotgunBullet"""

    def __init__(self):
        super().__init__(30)
        self.texture = WIMG
        self.texture_rectangle = B_SHOTGUN
        self.TTL = cfg['SHOTGUN_TTL']

    def update(self, level, om, dt):
        super().update(level, om, dt)
        if l(self.V) > 0:
            self.rotation = angle_d(self.V)

    def set_fire(self, direction):
        self.V = n(direction) * cfg['SHOTGUN_SPEED']
        return self

    def explode(self, om, reason):
        if reason is not None and issubclass(type(reason), Object):
            if (getattr(reason, 'hit', False)):
                reason.hit(cfg['SHOTGUN_ATK'], self.host)
        om.bury(self.id)


class GranaderBullet(Bullet):

    """GranaderBullet"""

    def __init__(self):
        super().__init__(30)
        self.TTL = 512
        self.texture = WIMG
        self.texture_rectangle = B_GRANADER
        self.M = 100 * 14 * pi * cfg['OBJ_DENSITY']
        self.exploded = 0

    def update(self, level, om, dt):
        super().update(level, om, dt)
        if l(self.V) > 0:
            self.rotation = self.TTL * 5

    def set_fire(self, direction):
        self.V = n(direction) * cfg['GRANADER_SPEED']
        return self

    def explode(self, om, reason):
        if self.exploded:
            return
        self.exploded = 1

        r = cfg['GRANADER_EXPLODE_RADIUS']
        p = self.position
        atk = cfg['GRANADER_ATK']
        for obj in om.get_objects():
            delta = obj.position - p
            delta_len = max(l(delta), obj.radius)

            if delta_len < r + obj.radius:
                danger_segment = (r - obj.radius)
                to_edge = delta_len - obj.radius
                mod = max(0, (danger_segment - to_edge) / danger_segment)

                if reason != self.host:
                    mod /= 3

                ep = cfg['GRANADER_EXPLODE_POWER']
                impulse_vector = n(delta) * self.M / delta_len / delta_len
                obj.apply_impulse(impulse_vector, ep)
                obj.hit(atk * mod, self.host)

        om.bury(self.id)
