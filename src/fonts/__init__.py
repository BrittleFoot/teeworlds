from os.path import join


def get(name):
    return join('src', 'fonts', name)
