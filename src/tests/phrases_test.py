import unittest


class UtilTester(unittest.TestCase):

    def test_phrases(self):
        import src.phrases as phrs

        d = phrs.get_dict('eng', 'server')._dic
        phraser = phrs.Phraser(d)

        for key, value in d.items():
            self.assertEqual(value, phraser[key])

        self.assertEqual(phraser['some'], 'some')


if __name__ == '__main__':
    unittest.main()
