import unittest


class UtilTester(unittest.TestCase):

    def test_clay(self):
        from src.util.clay import Clay

        item = Clay({'mother': 'Nadya', 'age': 30})

        self.assertEqual(item.age, 30)
        item.age = 20
        self.assertEqual(item.age, 20)

        i2 = Clay.deserialize(item.serialize())
        i2.fun = 'azaza'
        item.update(i2.serialize())
        self.assertEqual(item.age, 20)
        self.assertEqual(item.fun, i2.fun)

    def test_dict_pare(self):
        from src.util.dict_pare import DictPare

        d = DictPare('cid', 'name')
        d.add_name(3, 'petya')
        d.add_name(29, 'bob')

        self.assertTrue(d.get_name(3) == 'petya')
        self.assertTrue(d.get_cid('bob') == 29)

        d.del_cid('petya')
        d.del_name(29)

        with self.assertRaises(KeyError):
            d.__getitem__(3)
            d.__getitem__('bob')

    def test_events_and_mouse(self):
        from src.util.events import mKeyEvent, mMouseEvent
        from src.util.mouse import Cursor
        # nothing to test
        self.assertEqual(mKeyEvent(True, 1).key, 1)
        self.assertEqual(mMouseEvent(True, 1, (10, 20)).position, (10, 20))

        c = Cursor('window', 'vmode', 'view')

    def test_timer(self):
        from src.util.timer import Timer, sleep

        called = 0

        def function(time):
            nonlocal called
            self.assertTrue("I_HAS_BEEN_CALLED!")
            called = 1

        t = Timer(function, 1/100).start()
        sleep(1/100)
        t.stop()

        self.assertTrue(called)


if __name__ == '__main__':
    unittest.main()
