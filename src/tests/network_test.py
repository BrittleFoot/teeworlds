import logging as log
import unittest
loghead = '%(levelname)7s: %(module)s: '
logbody = '%(funcName)s %(lineno)3d> %(message)s'
log.basicConfig(level=log.FATAL,
                format=loghead + logbody)


class fakeRecvSocket():

    def __init__(self, bytestr):
        self.bytestr = bytestr
        self.index = 0

    def recv(self, n):
        res = self.bytestr[self.index: self.index + n]
        self.index += n
        return res


class NetworkTests(unittest.TestCase):

    def test_protocol(self):
        from src.network.network_proto import make_command, recv_command_from
        from random import randint

        # One large command!
        args = b'\x00' * 1024 * 1024
        data = make_command(b'\x00', args)
        sock = fakeRecvSocket(data)

        recvd_cmd, recvd_args = recv_command_from(sock)

        self.assertEqual(len(args), len(recvd_args))

        # A lot of small commands with trash

        TESTCOUNT = 10000

        bigcmds = b''
        for x in range(TESTCOUNT):
            bigcmds += make_command(b'\x00', str(x))
            bigcmds += b'\x00' * randint(0, 100)

        sock = fakeRecvSocket(bigcmds)

        res = []

        while 1:
            try:
                res.append(recv_command_from(sock))
            except ConnectionAbortedError:
                break

        self.assertEqual(len(res), TESTCOUNT)

    def test_protocol_unstandart_case(self):
        from src.network.network_proto import make_command, recv_command_from

        cmd = b'\x00', b'123123'

        # recv_command_from(...) считвыает по 1024 байта
        # такие числа в тесте моделируют ситуацию, когда метка команды
        # оказалась концом, серединой, началом нового сообщения
        for pushed_bytes in {1022, 1023, 1024}:
            bigcmds = b'\x00' * pushed_bytes + make_command(*cmd)
            sock = fakeRecvSocket(bigcmds)

            res = []

            while 1:
                try:
                    res.append(recv_command_from(sock))
                except ConnectionAbortedError:
                    break

            self.assertEqual(res[0], cmd)

    def test_client_server_communication(self):
        from src.network.network_client import Client
        from src.network.network_server import Server
        from time import sleep

        print('\nConfigurating server...')
        server = Server()
        server.start()

        srv_messages = set()

        def server_handle(cmd, message, cid):
            nonlocal srv_messages
            if message:
                srv_messages.add(message)
            log.debug('Handling "%s"' % message)
            server.send_to(cid, cmd, message)

        server.handle = server_handle

        ######################################

        print('Connecting to the server...')
        client = Client()
        client.bind()
        client.connect('127.0.0.1', server.port)

        clnt_messages = set()

        def client_handle(cmd, message):
            nonlocal clnt_messages
            if message:
                clnt_messages.add(message)
            log.debug('Handling ' + str(message))

        client.handle = client_handle

        messages = [
            'hi',
            'how',
            'are',
            'you'
        ]
        sleep(1/10)
        try:
            for mess in messages:
                if mess:
                    client.send(b'\x00', mess)

            print('Waiting messaging time...')
            sleep(1)
        finally:
            client.stop()
            server.stop()
            print('Done!')

        self.assertEqual(clnt_messages, srv_messages)

    def test_many_client_server_communication(self):
        from src.network.network_client import Client
        from src.network.network_server import Server
        from time import sleep

        print('\nConfigurating server...')
        server = Server()
        server.start()

        srv_messages = set()

        def server_handle(cmd, message, cid):
            nonlocal srv_messages
            if message:
                srv_messages.add(message)
            log.debug('Handling "%s"' % message)
            server.send_to_anyone(cmd, message)

        server.handle = server_handle

        ######################################

        print('Connecting to the server...')
        client = Client()
        client.bind()
        client.connect('127.0.0.1', server.port)

        clnt_messages = set()

        def client_handle(cmd, message):
            nonlocal clnt_messages
            if message:
                clnt_messages.add(message)
            log.debug('Handling ' + str(message))

        client.handle = client_handle

        ######################################
        cluent = Client()
        cluent.bind()
        cluent.connect('127.0.0.1', server.port)

        cluent_messages = set()

        def client_handle(cmd, message):
            nonlocal cluent_messages
            if message:
                cluent_messages.add(message)
            log.debug('Handling ' + str(message))

        cluent.handle = client_handle

        sleep(1/10)
        messages = [
            'hi',
            'how',
            'are',
            'you'
        ]
        try:
            for mess in messages:
                if mess:
                    client.send(b'\x00', mess)

            print('Waiting messaging time...')
            sleep(1)
        finally:
            client.stop()
            cluent.stop()
            server.stop()
            print('Done!')

        self.assertEqual(clnt_messages, cluent_messages)


if __name__ == '__main__':

    unittest.main()
