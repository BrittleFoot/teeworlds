import src.level_tools as lvl
import unittest

from SFML import Vector2
from src.deserializer import deserialize
from src.network.commands_codes import CODE
from src.network.commands_codes import SPLITTER
from src.object_manager import ComparableObjectManager
from src.object_manager import ObjectManager
from src.physical_engine import PhysicalEngine
from src.serializer import serialize
from src.tee import PseudoTee
from src.tee import Tee
from src.util.events import mKeyEvent
from src.util.events import mMouseEvent

import src.ammo as ammo


class NamedInteger:

    def __init__(self, name):
        self.name = name

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return hash(self.name)


class OM_TESTING(unittest.TestCase):

    def test_add_events_and_enumerate(self):
        """
            тест на потокобезопасность эвент-ответственной части
        """
        om = ObjectManager('some_fake_of_level')
        for obj_fake in range(50):
            om.add(NamedInteger(str(obj_fake)))

            for ev in range(10):
                om.add_event(str(obj_fake), ev)

        i = 0
        for obj, event in om.poil_events():
            om.add_event(obj.name, event * 10)
            i += 1

        self.assertTrue(i == 50 * 10)
        self.assertTrue(len(list(om.poil_events())) == 50 * 10)

    def test_omanager_and_comp_omanager_with_tee_and_ammo_on_level(self):
        level = lvl.Level.load_example()
        level = lvl.Level.load_from_file('example')
        om = ObjectManager(level)
        pm = PhysicalEngine(level, om)

        for i in range(10):
            tee = Tee()
            tee.position = (10 * i, 10. * i)
            tee.name = str(i)
            om.add(tee)
            om.add_weapon(ammo.Hammer((10 * i, 10 * i)))
            om.add_weapon(ammo.Gun((10 * i, 10 * i)))
            om.add_weapon(ammo.Shotgun((10 * i, 10 * i)))
            om.add_weapon(ammo.Granader((10 * i, 10 * i)))

            ev = [
                mKeyEvent(True, CODE.LEFT),
                mKeyEvent(True, CODE.RIGHT),
                mKeyEvent(True, CODE.RIGHT),
                mMouseEvent(True, CODE.RMOUSE, Vector2(-50, 40)),
                mMouseEvent(True, CODE.LMOUSE, Vector2(24, 537)),
                mKeyEvent(True, CODE.JUMP)
            ]
            om.add_event(tee.name, ev[i % len(ev)])

        om.update(10)
        pm.check_coll(10)

        for weap in om.get_weapons():
            if weap.host is not None and not isinstance(weap.host, str):
                weap.shoot(Vector2(10, 10))

        om.update(10)
        pm.check_coll(10)

        packed = SPLITTER.join(om.pack_to_only_drawable_format())

        co_om = ComparableObjectManager(packed)

        om.update(10)
        pm.check_coll(10)

        co_om = ComparableObjectManager(packed)


def main():
    unittest.main()


if __name__ == '__main__':
    main()
