#!/usr/bin/env python3
import SFML as sf

from SFML import Color
from SFML import Font
from SFML import RectangleShape
from SFML import Text
from SFML import Vector2
from threading import Lock

INTERVAL = (0, 40)  # in pixels
TIMEOUT = 5 * 1000000  # in microseconds
FONT_PATH = 'src/fonts/monof55.ttf'


class LogString(object):

    """docstring for LogString"""

    def __init__(self, data, color=Color.WHITE):
        self.text = Text(font=Font.from_file(FONT_PATH))
        self.text.character_size = 30
        self.data = data
        self.lifetime = 0
        self.color = Color(*color)
        self.height = len(self.data.split('\n'))

    def update(self, time, show_fullcolor):
        self.text.string = self.data

        if not show_fullcolor:
            self.lifetime += time

        if self.lifetime > TIMEOUT:
            self.lifetime = TIMEOUT

        opacity = self.lifetime / TIMEOUT
        opacity = [round(255 - opacity * 255), 255][show_fullcolor]

        self.color.a = opacity
        self.text.color = self.color


class Log(object):

    """docstring for Log"""

    def __init__(self, window, max_count, view, console):
        self.view = view
        self.strings = list()
        self.console = console
        self.count = max_count
        self.window = window
        self.stringlock = Lock()

    def writeline(self, line, color=Color.WHITE):
        with self.stringlock:
            line = str(line)
            log_line = LogString(line, color)

            self.strings.append(log_line)
            self.strings = self.strings[-self.count:]

    def update(self, time, show_fullcolor=False):
        with self.stringlock:
            i = 1
            position = self.console.position + self.view.corner
            for log_line in self.strings[::-1]:
                log_line.text.position = position + Vector2(*INTERVAL) * i
                log_line.update(time, show_fullcolor)
                i += log_line.height

    def draw(self):
        with self.stringlock:
            for log_line in self.strings:
                self.window.draw(log_line.text)
