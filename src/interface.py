import SFML as sf

from src.ammo import B_GRANADER
from src.ammo import B_GUN
from src.ammo import B_SHOTGUN
from src.ammo import R_HAMMER
from src.physic_primtives import cfg

from src.data import get_img
IMG = sf.Texture.from_file(get_img('game.png'))
IMG.smooth = True
HP_RECT = sf.Rectangle((672, 0), (64, 64))
HP_EMPTY_RECT = sf.Rectangle((672 + 64, 0), (64, 64))


SIZE = sf.Vector2(64, 64)


class Interface:

    """Pannel for displayeing current tee health and ammo"""

    def __init__(self, view):
        self.view = view
        self.position = (0, 0)

        self.hp = sf.RectangleShape()
        self.hp.texture = IMG
        self.hp.texture_rectangle = HP_RECT
        self.hp.size = SIZE

        self.hp_empty = sf.RectangleShape()
        self.hp_empty.texture = IMG
        self.hp_empty.texture_rectangle = HP_EMPTY_RECT
        self.hp_empty.size = SIZE

        self.hammer = sf.RectangleShape()
        self.hammer.texture = IMG
        self.hammer.texture_rectangle = R_HAMMER
        self.hammer.size = SIZE

        self.gun = sf.RectangleShape()
        self.gun.texture = IMG
        self.gun.texture_rectangle = B_GUN
        self.gun.size = SIZE

        self.shotgun = sf.RectangleShape()
        self.shotgun.texture = IMG
        self.shotgun.texture_rectangle = B_SHOTGUN
        self.shotgun.size = SIZE

        self.granader = sf.RectangleShape()
        self.granader.texture = IMG
        self.granader.texture_rectangle = B_GRANADER
        self.granader.size = SIZE

        self.weap = {
            'Hammer': self.hammer,
            'Gun': self.gun,
            'Shotgun': self.shotgun,
            'Granader': self.granader
        }

    def apply_n_draw(self, tee, window):
        if tee is None:
            return

        self.position = self.view.corner + self.view.view.size * (0.1, 0.8)

        max_hp = int(cfg['MAX_HEALTH'])
        tee_hp = int(round(tee.health))

        pos = self.position
        wpos = self.position + (0, SIZE.y)

        for i in range(max_hp):
            p = pos + (SIZE.x * i, 0)
            wp = wpos + (SIZE.x * i, 0)

            hp = [self.hp, self.hp_empty][i >= tee_hp]
            hp.position = p
            window.draw(hp)

            self.weap[tee.curr_weap].position = wp
            if i < tee.curr_ammo:
                window.draw(self.weap[tee.curr_weap])
