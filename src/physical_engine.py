#!/usr/bin/env python3
from src.object_manager import ObjectManager
from src.physic_primtives import Collision
from src.physic_primtives import Object
from src.physic_primtives import cfg


class PhysicalEngine():

    """docstring for PhysicalEngine"""

    def __init__(self, level, omanager):
        self.level = level
        self.omanager = omanager

    def check_coll(self, dt):
        for obj1 in self.omanager.get_objects():
            for obj2 in self.omanager.get_others(obj1):
                obj1.check_coll(obj2)
            for bullet in self.omanager.get_bullets():
                if obj1 != bullet.host and Collision.balls_intersects(obj1, bullet):
                    bullet.explode(self.omanager, obj1)
            for weap in self.omanager.get_free_weapons():
                if Collision.intersects(obj1, weap.global_bounds):
                    obj1.pickup_weapon(weap)
