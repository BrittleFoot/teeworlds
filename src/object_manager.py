from random import choice
from src.deserializer import deserialize
from src.network.commands_codes import SPLITTER
from src.serializer import serialize
from src.tile_types import *
from threading import Lock


class ObjectManager():

    """docstring for ObjectManager"""

    def __init__(self, level):
        self.level = level
        self.objects = {}  # name : object
        self.events = {}  # object : [event]
        self.bullets = {}  # weapon_id : bullet
        self.weapons = {}  # weapon_id : weapon

        self.recycle_bin = set()

        self.lock = Lock()
        self.event_lock = Lock()

    def __iter__(self):
        yield from self.get_objects()
        yield from self.get_bullets()
        yield from self.get_weapons()

    def get_objects(self):
        obj = list(self.objects.values())
        yield from obj

    def get_others(self, obj):
        yield from (other for other in self.get_objects() if other != obj)

    def get_bullets(self):
        bull = list(self.bullets.values())
        yield from bull

    def get_weapons(self):
        weap = list(self.weapons.values())
        yield from weap

    def get_free_weapons(self):
        yield from (weap for weap in self.get_weapons() if weap.host is None)

    def add(self, object):
        with self.lock:
            self.objects[object.name] = object
            self.events[object] = []

    def add_weapon(self, weapon):
        with self.lock:
            self.weapons[weapon.id] = weapon

    def add_bullet(self, bullet):
        with self.lock:
            self.bullets[bullet.id] = bullet

    def add_event(self, obj_name, event):
        if obj_name in self.objects:
            self.events[self[obj_name]].append(event)

    def poil_events(self):
        for obj, events in list(self.events.items()):
            ev = events[::]
            self.events[obj] = []
            yield from ((obj, event) for event in events)

    def bury(self, some_one):
        self.recycle_bin.add(some_one)

    def empty_trash(self):
        recycle_bin = self.recycle_bin
        self.recycle_bin = set()
        with self.lock:
            for item in recycle_bin:
                obj = self.objects.pop(
                    item, self.bullets.pop(
                        item, self.weapons.pop(
                            item, None
                        )
                    )
                )
                self.events.pop(obj, None)

    def __getitem__(self, key):
        return self.objects.get(key, self.bullets.get(key, None))

    def update(self, dt):
        for obj, event in self.poil_events():
            obj.em.handle(event)

        for obj in self:
            obj.update(self.level, self, dt)

    def draw(self, window):
        for obj in self:
            obj.draw(window)

    def pack_to_only_drawable_format(self):
        yield from (serialize(obj) for obj in self)


class ComparableObjectManager:

    """
    Data structure for saveing object 
    and compare them by id in couple of the same structures
    """

    def __init__(self, data):
        """
        data - splitted by SPLITTER serialized objects. 
        """
        self.objects = {"Tee": {}, "Oth": {}}
        self.tee = {}  # Name:Tee
        for obj in data.split(SPLITTER):
            unpacked = deserialize(obj)
            category = ["Oth", "Tee"][unpacked.type == "Tee"]
            self.objects[category][unpacked.id] = unpacked
            if category == "Tee":
                self.tee[unpacked.name] = unpacked

    def draw(self, window):

        for tee in self.objects["Tee"].values():
            tee.draw(window)
        for oth in self.objects["Oth"].values():
            oth.draw(window)

    def iterate_with(self, com):

        for tee in self.objects["Tee"].values():
            yield (tee, com.objects["Tee"].get(tee.id, None))
        for oth in self.objects["Oth"].values():
            yield (oth, com.objects["Oth"].get(oth.id, None))
