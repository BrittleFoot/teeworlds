#!/usr/bin/env python3
import logging as log

from SFML import Clock
from SFML import Color
from SFML import Rectangle
from SFML import RectangleShape
from SFML import Vector2
from collections import deque
from src.info_line import InfoLine
from src.interface import Interface
from src.level_tools import Level
from src.level_tools import Tile
from src.network.commands_codes import CODE
from src.network.commands_codes import SPLITTER
from src.object_manager import ComparableObjectManager
from src.tileset import Tileset
from src.util.clay import Clay
from src.util.timer import Timer
from threading import Lock
from time import sleep

UPDATE_FREQ = 1 / 66
CLIENT_DELAY = 75 * 1000  # in microseconds
ENDL = '\r%s\r' % ' ' * 10
STORED_SNAPSHOTS = 10

MAP_CODES = {
    CODE.MAP_HEADER,
    CODE.MAP_DATE,
    CODE.MAP_DATE_END
}
OBJ_CODES = {
    CODE.OBJ_HEADER,
    CODE.OBJ_DATA,
    CODE.OBJ_DATA_END
}
MESSAGES = {
    CODE.MESSAGE,
    CODE.INFO,
    CODE.ERROR
}


class DrowDataState(object):

    """docstring for DrowDataState"""

    def __init__(self, window, view, log, cursor):
        self.window = window
        self.view = view
        view.view.size *= 1.5
        self.log = log
        self.level = Level.empty()
        self.target = ''
        self.update_timer = Timer(self.update, UPDATE_FREQ)

        self.data_lock = Lock()
        self.data_clock = Clock()

        self.temp_snap = b''
        self.snaps = deque([])
        self.snap = None
        self.snap_counter = 0

        self.interface = Interface(view)
        self.info = InfoLine.make(window, cursor, [
            'name',
            'health',
            'score'
        ])

        self.my_name = ''

    def resolve(self, command, args):

        if command in OBJ_CODES:
            self.get_snap(command, args)

        elif command in MESSAGES:
            self.get_message(command, args)

        elif command == CODE.NAME:
            self.target = args.decode()

        elif command == CODE.DISCONNECTED:
            name = args.decode()

        elif command in MAP_CODES:
            self.get_map(command, args)

    def get_snap(self, command, args):
        if command == CODE.OBJ_HEADER:
            self.temp_snap = b''

        elif command == CODE.OBJ_DATA:
            self.temp_snap += args

        elif command == CODE.OBJ_DATA_END:
            control_length = int(args.decode())
            if control_length == len(self.temp_snap):
                snap = ComparableObjectManager(self.temp_snap)
                dt = self.data_clock.elapsed_time.microseconds
                if dt > 2 * CLIENT_DELAY:
                    dt = CLIENT_DELAY
                snap.dt = dt
                self.data_clock.restart()

                with self.data_lock:
                    self.snaps.appendleft(snap)
                    if len(self.snaps) > STORED_SNAPSHOTS:
                        self.snaps.pop()

                self.snap_counter += 1
            else:
                info_args = (control_length, len(self.temp_snap))
                info = 'expect %s, got %s length data :(' % info_args
                log.error('Invalid snap: controll summ corrupted. ' + info)

    def get_map(self, command, args):
        if command == CODE.MAP_HEADER:
            self.level.name, tileset = args.decode().split("|")
            self.level.tileset = Tileset(tileset)
        elif command == CODE.MAP_DATE:
            while args:
                tle = Tile.deserialize(args[0:Tile.LENGTH_SERIALIZED])
                self.level.add_tile(tle)
                args = args[Tile.LENGTH_SERIALIZED:]
        elif command == CODE.MAP_DATE_END:
            self.level.empty = False

    def get_message(self, command, args):

        if command == CODE.MESSAGE:
            self.log.writeline(args.decode())
        elif command == CODE.INFO:
            self.log.writeline(args.decode(), Color.YELLOW)
        elif command == CODE.ERROR:
            self.log.writeline(args.decode(), Color.RED)

    def update(self, time):
        if self.snap:
            self.view.target = self.snap[0].tee.get(self.target, None)

    def draw(self):

        self.interpolation()
        print(self.snap_counter, end="\r\t\t\r")
        with self.data_lock:
            if self.snap is not None:
                for snap in self.snap:
                    snap.draw(self.window)
                    for tee in snap.tee.values():
                        self.info.draw_about(tee)

        self.level.draw(self.window)

        if self.target is not None and self.snap:
            tee = self.snap[0].tee.get(self.target, None)
            self.interface.apply_n_draw(tee, self.window)

    def interpolation(self):

        if self.snaps:
            self.snap = [self.snaps[0]]

        from SFML import Keyboard as k

        if k.is_key_pressed(k.I):
            # покажет разницу между интерполированой картинкой и реальной
            return

        offset = self.data_clock.elapsed_time.microseconds

        time_sum = 0
        with self.data_lock:
            snaps = iter(self.snaps)
            if len(self.snaps) > 5:
                try:
                    for snap in snaps:
                        if offset + time_sum + snap.dt > CLIENT_DELAY:
                            self.snap = [next(snaps), snap]
                            break

                        time_sum += snap.dt
                except StopIteration:
                    pass

        if self.snap and len(self.snap) == 2:
            snap, lead = self.snap
            odt = time_sum + lead.dt + offset - CLIENT_DELAY

            if odt < lead.dt:
                offset = odt
            else:
                self.snap = [lead]
                return

            dt = lead.dt

            def new_pos(pos, lpos): return (lpos - pos) * offset / dt + pos

            for obj, lead_obj in snap.iterate_with(lead):
                if lead_obj is not None:
                    obj.position = new_pos(obj.position, lead_obj.position)

            self.snap = [snap]
