#!/usr/bin/env python3
import logging as log
import sys

from SFML import CircleShape
from SFML import Keyboard as key
from SFML import Rectangle
from SFML import RectangleShape
from SFML import Texture
from SFML import Vector2
from src.ammo import WIMG
from src.flexable import FlexableEntity
from src.flexable import init_from_dict
from src.network.commands_codes import CODE
from src.physic_primtives import HORIZONTAL
from src.physic_primtives import Hook
from src.physic_primtives import Object
from src.physic_primtives import VERTICAL
from src.physic_primtives import cfg
from src.tee_texture_manager import draw as draw_skin
from src.tile_types import *
from src.util.events import mKeyEvent
from src.util.events import mMouseEvent
from src.vector_extends import angle_d
from src.vector_extends import l
from src.vector_extends import rotate_d


weapon_number = {
    key.NUM1: "Hammer",
    key.NUM2: "Gun",
    key.NUM3: "Shotgun",
    key.NUM4: "Granader"
}


class TeeEventSolver:

    """handle and provide all events for tee"""

    def __init__(self, target):
        self.tee = target
        self.key = dict()
        self.button = dict()

        # В евенте передовать позцию курсора от (0, 0).
        self.mpos = Vector2()

    def handle(self, event):
        if event.__class__.__name__ == 'mKeyEvent':
            self.key[event.key] = event.pressed
            [self.key_released, self.key_pressed][event.pressed](event.key)
        elif event.__class__.__name__ == 'mMouseEvent':
            self.button[event.button] = event.pressed
            self.mpos = event.position - self.tee.position
            [self.mb_released, self.mb_pressed][event.pressed](event.button)

    def is_key_pressed(self, key):
        return self.key.get(key, False)

    def is_button_pressed(self, key):
        return self.button.get(key, False)

    def key_pressed(self, key):

        if key == CODE.JUMP and self.tee.can_jump > 0:
            self.tee.can_jump -= 1
            self.tee.apply_impulse(Vector2(0, -10), cfg['JUMP_POWER'])
        if key in weapon_number:
            wname = weapon_number[key]
            in_arm = wname in self.tee.arm
            self.tee.curr_weap = [self.tee.curr_weap, wname][in_arm]

    def key_released(self, key):
        pass

    def mb_pressed(self, button):
        if button == CODE.RMOUSE:
            self.tee.throw_hook(self.mpos)
        if button == CODE.LMOUSE:
            if self.tee.curr_weap is not None:
                self.tee.arm[self.tee.curr_weap].shoot(self.mpos)

    def mb_released(self, button):
        pass


class Tee(Object):

    """docstring for Tee"""

    def __init__(self):
        super(Tee, self).__init__(int(32 * 1.3))
        self.em = TeeEventSolver(self)
        self.name = "nameless_tee%s" % self.id
        self.hook = None
        self.arm = dict()
        self.can_jump = 0
        self.curr_weap = None
        self.is_dead = False
        self.hitted = 0
        self.health = cfg['MAX_HEALTH']
        self.score = 0
        self.skin = 'default'

    def hit(self, value, insuler):
        self.health = max(0, self.health - value)
        self.is_dead = not self.health
        self.hitted = 100
        self.health = round(self.health, 2)

        if hasattr(insuler, 'score'):
            self_kik = [1, -1/3][self.id == insuler.id]
            insuler.score = round(insuler.score + value * self_kik, 2)
            if not self.health and self_kik == 1:
                insuler.score += 10 + max(self.score, 0)

        return self.health

    def throw_hook(self, mpos):
        if self.hook is None:
            self.hook = Hook(self, mpos)

    def update(self, level, obj_list, dt):
        if self.hitted > 0:
            self.hitted -= 1
        super().update(level, dt)
        pressed_keys = (k for k, v in self.em.key.items() if v)

        for key in pressed_keys:
            if key == CODE.LEFT:
                self.apply_impulse(Vector2(-10, 0), cfg['MOVE_SPEED'])
            if key == CODE.RIGHT:
                self.apply_impulse(Vector2(10, 0), cfg['MOVE_SPEED'])

        if self.hook is not None:
            pressed = self.em.is_button_pressed(CODE.RMOUSE)
            if self.hook.tick(obj_list, level, pressed):
                self.hook = None

        if self.on_ground:
            self.can_jump = 1

        if not self.health:
            obj_list.bury(self.name)
            for weapon in self.arm.values():
                if self.curr_weap == weapon.name:
                    weapon.host = None
                    weapon.rotation = 0
                else:
                    obj_list.bury(weapon.id)

    def draw(self, window):
        if self.hook is not None:
            self.hook.draw(window)
        super().draw(window)

    def solve_collision(self, tile_rect, tile_type, direct):
        super().solve_collision(tile_rect, tile_type, direct)
        if tile_type == THORNS:
            self.hit(10, 'god')
        if tile_type == SOLID and direct == VERTICAL:
            pass

    def pickup_weapon(self, weapon):
        if weapon.name in self.arm:
            weapon.host = 'dead_man'
            self.arm[weapon.name].reload()
        else:
            weapon.host = self
            self.curr_weap = weapon.name
            self.arm[weapon.name] = weapon


class Pseudo(CircleShape):

    """ Beware, both instances and inheritors of this class
        should be initialized only with the .from_dictionary method!

        Just I hate to make incapsulation, too many letters :)
    """

    def __init__(self):
        super(Pseudo, self).__init__()

    @classmethod
    def from_dictionary(cls, d):
        self = cls()
        self.radius
        init_from_dict(self, d)
        return self


b_hook = RectangleShape()
b_hook.size = (32, 32)
b_hook.origin = (0, 16)
b_hook.texture = WIMG
b_hook.texture_rectangle = Rectangle((64, 0), b_hook.size)
h_hook = RectangleShape()
h_hook.size = (64, 32)
h_hook.origin = (0, 16)
h_hook.texture = WIMG
h_hook.texture_rectangle = Rectangle((96, 0), h_hook.size)


class PseudoTee(Pseudo):

    """docstring for PseudoTee"""

    def __init__(self):
        super(PseudoTee, self).__init__()

    def draw(self, window):
        if self.hook is not None:
            angle = angle_d(self.hook)
            hlen = l(self.hook)

            b_hook.rotation = angle
            h_hook.rotation = angle

            hlen -= h_hook.size.x / 4 * 3

            start_pos = self.position
            direction = rotate_d(Vector2(1, 0), angle)
            pos = start_pos + direction * hlen

            h_hook.position = pos
            window.draw(h_hook)

            while hlen > 0:
                hlen -= 32
                pos = start_pos + direction * hlen
                b_hook.position = pos
                window.draw(b_hook)

        draw_skin(window, self)


def main():
    print('tee.__main__')
    tee = Tee()
    print('tee.created')


if __name__ == '__main__':
    main()
