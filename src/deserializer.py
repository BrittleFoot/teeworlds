import src.ammo as Ammo
import src.tee

from SFML import CircleShape
from SFML import RectangleShape
from SFML import Vector2
from pickle import loads
from src.flexable import FlexableEntity
from src.tee import PseudoTee


def deserialize_tee(dtee):

    tee = PseudoTee.from_dictionary(dtee)
    return tee


def deserialize_ammo(dammo):
    arg = [dammo['position'], None]['radius' in dammo]
    if arg:
        ammo = getattr(Ammo, dammo['type'])(arg)
    else:
        ammo = getattr(Ammo, dammo['type'])()
    FlexableEntity.__init__(ammo, dammo)
    ammo.update("just_update_me", 0, 0)
    return ammo


def deserialize(jsmth):
    try:
        dsmth = loads(jsmth)
    except Exception as e:
        print("Serialize error: %s" % e)
        return None

    if dsmth['type'] == "Tee":
        return deserialize_tee(dsmth)
    else:
        return deserialize_ammo(dsmth)
