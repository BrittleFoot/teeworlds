#!/usr/bin/env python3

NONE = 0
SOLID = 1
THORNS = 2
SPAWN = 3
WSPAWN = 4

TILESIZE = 64
