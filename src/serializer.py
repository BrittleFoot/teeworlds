from pickle import dumps
from src.ammo import Bullet
from src.ammo import Granader
from src.ammo import GranaderBullet
from src.ammo import Gun
from src.ammo import GunBullet
from src.ammo import Hammer
from src.ammo import Shotgun
from src.ammo import ShotgunBullet
from src.ammo import Weapon
from src.tee import Hook
from src.tee import Tee


assert(issubclass(Tee, Tee))
assert(issubclass(GranaderBullet, Bullet))
assert(issubclass(Granader, Weapon))


def serialize_tee(tee):
    return dumps({
        "type": tee.__class__.__name__,
        "name": tee.name,
        "id": tee.id,
        "position": list(tee.position),
        "speed": list(tee.V),
        "mpos": list(tee.em.mpos),
        "origin": list(tee.origin),
        "radius": tee.radius,
        "curr_weap": tee.curr_weap,
        "curr_ammo": None if not tee.curr_weap else tee.arm[tee.curr_weap].ammo,
        "arm": list(tee.arm),
        "health": tee.health,
        "can_jump": tee.can_jump,
        "hitted": tee.hitted,
        "hook": None if tee.hook is None else list(tee.hook.normal),
        "view_point": list(tee.em.mpos),
        "score": tee.score,
        "on_ground": tee.on_ground,
        "skin": tee.skin
    })


def serialize_gun(gun):
    return dumps({
        "type": gun.__class__.__name__,
        "id": gun.id,
        "position": list(gun.position),
        "rotation": gun.rotation,
        "origin": list(gun.origin),
        "size": list(gun.size),
        "ammo": gun.ammo,
        "texture_rot": gun.texture_rot,
        "host": None if gun.host is None or isinstance(gun.host, str) else {
            "curr_weap": gun.host.curr_weap
        }
    })


def serialize_bullet(bullet):
    return dumps({
        "type": bullet.__class__.__name__,
        "id": bullet.id,
        "position": list(bullet.position),
        "rotation": bullet.rotation,
        "origin": list(bullet.origin),
        "radius": bullet.radius
    })


def serialize(smth):
    cls = smth.__class__.__name__
    if cls == "Tee":
        return serialize_tee(smth)
    elif cls in {"Weapon", "Hammer", "Gun", "Shotgun", "Granader"}:
        return serialize_gun(smth)
    else:
        return serialize_bullet(smth)
