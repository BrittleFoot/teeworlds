#!/usr/bin/env python3
import SFML as sf
import src.util.pyperclip as clipboard

from SFML import Color
from SFML import Font
from SFML import Keyboard as key
from SFML import RectangleShape
from SFML import Text
from SFML import Vector2
from os.path import join
from queue import Empty
from threading import Event

from src.fonts import get as fontget
FONT_PATH = fontget('monof55.ttf')


class Console(object):

    """docstring for Console"""

    def __init__(self, window, vmode, view):
        self.view = view
        self.window = window
        self.vmode = vmode
        self.message_event = Event()

        self.hist = list()
        with open(join('src', 'default_hist.txt'), mode='r') as f:
            self.hist = f.read().split('\n')
        self.hist_pointer = len(self.hist)

        self.textbox = Text()
        self.textbox.character_size = 35
        self.textbox.font = Font.from_file(FONT_PATH)
        self.inputstr = ''

        self.rect = RectangleShape((view.width * 0.9, 40))

        self.textbox.origin = self.rect.origin = Vector2(self.rect.size.x / 2, -10)
        self.rposition = Vector2(view.width / 2, 0)
        self.tposition = Vector2(view.width / 2 + 7, -5)
        self.rect.fill_color = Color(130, 255, 100, 150)

        self.update(0)

        self.position = self.rposition - self.rect.origin
        self.height = self.rect.size.y
        self.width = self.rect.size.x

    def check_key(self, event):
        code = event['code']

        if code == key.RETURN:
            self.message_event.set()

        elif code == key.ESCAPE:
            self.inputstr = ''
            self.message_event.set()

        elif code == key.UP and self.hist_pointer > 0:
            self.hist_pointer -= 1
            self.inputstr = self.hist[self.hist_pointer]

        elif code == key.DOWN and self.hist_pointer < len(self.hist) - 1:
            self.hist_pointer += 1
            self.inputstr = self.hist[self.hist_pointer]

        elif code == key.BACK_SPACE:
            self.inputstr = self.inputstr[:-1]

        elif event['control'] and code == key.V:
            self.inputstr += clipboard.paste()

        elif event['control'] and code == key.C:
            clipboard.copy(self.inputstr)

    def text_input(self, event):
        # 0x00 - 0x1f useless special unicode symbols. Let`s ignore them.
        if ord(event['unicode']) > 0x1f:
            self.inputstr += event['unicode']

    def update(self, time):
        self.rect.size = (self.view.width * 0.9, 40)
        self.textbox.position = self.tposition + self.view.corner
        self.rect.position = self.rposition + self.view.corner
        self.textbox.string = self.inputstr
        self.position = self.rposition - self.rect.origin

    def draw(self):
        self.window.draw(self.rect)
        self.window.draw(self.textbox)

    def send(self, message):
        self.inputstr = message
        self.message_event.set()

    def readline(self, TIMEOUT):
        if self.message_event.wait(TIMEOUT):
            self.message_event.clear()

            line = self.inputstr
            self.inputstr = ''
            if line:
                if not self.hist or line != self.hist[-1]:
                    self.hist.append(line)
            self.hist_pointer = len(self.hist)
            return line
        else:
            raise Empty


def main():
    pass


if __name__ == '__main__':
    main()
