#!/usr/bin/env python3
import threading as t


def call_async(func, *arguments):
    """called function in new thred with specific arguments

    # Important:
    #   Эта функция создает поток, но не говорит системе о том,
    #   что поток умер. Хорошо ли?..
    """
    thred = t.Thread(target=func, args=arguments)
    thred.start()
