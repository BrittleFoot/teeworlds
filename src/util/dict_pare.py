#!/usr/bin/env python3
from threading import Lock


class DictPare(object):

    """docstring for DictPare"""

    def __init__(self, name1, name2):
        self._dict1 = dict()
        self._dict2 = dict()

        self._lock = Lock()

        self._n1 = name1
        self._n2 = name2

        self.__setattr__('add_%s' % name1, self._add_name1)
        self.__setattr__('add_%s' % name2, self._add_name2)

        self.__setattr__('del_%s' % name1, self._del_name1)
        self.__setattr__('del_%s' % name2, self._del_name2)

        self.__setattr__('get_%s' % name1, self._dict1.__getitem__)
        self.__setattr__('get_%s' % name2, self._dict2.__getitem__)

    def _add_name1(self, key, value):

        with self._lock:
            if key in self._dict1:
                raise KeyError("%s '%s' already exist." % (self._n2, key))
            if value in self._dict2:
                raise KeyError("%s '%s' already exist." % (self._n1, value))

            self._dict1[key] = value
            self._dict2[value] = key

    def _add_name2(self, key, value):
        self._add_name1(value, key)

    def _del_name1(self, name2):
        with self._lock:
            if name2 in self._dict1:
                value = self._dict1.pop(name2)
                self._dict2.pop(value)
                return value
            else:
                return 0

    def _del_name2(self, name1):
        with self._lock:
            if name1 in self._dict2:
                value = self._dict2.pop(name1)
                self._dict1.pop(value)
                return value
            else:
                return 0

    def __getitem__(self, name):
        if name in self._dict1:
            return self._dict1[name]
        elif name in self._dict2:
            return self._dict2[name]
        else:
            raise KeyError(name)

    def __str__(self):
        with self._lock:
            head = str((self._n1, self._n2)) + '\n'
            return head + '\n'.join(str(p) for p in self._dict2.items())


def main():
    pass


if __name__ == '__main__':
    main()
