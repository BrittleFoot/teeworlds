#!/usr/bin/env python3
from SFML import Mouse
from SFML import RectangleShape
from SFML import Vector2


class Cursor(object):

    """docstring for Mouse"""

    def __init__(self, window, vmode, view):
        self.window = window
        self.view = view
        self.vmode = vmode
        self.enabled = 0
        self.size = 40

        self.position = Vector2(0, 0)
        self.cursor = RectangleShape((self.size, self.size))
        self.cursor.origin = Vector2(self.size, self.size)

    def get_position(self):
        return self.position

    def update(self, time):
        mpos = Mouse.get_position(self.window)
        self.position = mpos * self.view.ratio + self.view.corner
        self.cursor.position = self.position

    def is_pressed(self, button=0):
        return Mouse.is_button_pressed(button)

    @property
    def radius(self):
        return self.size / 2
    

    def draw(self):
        self.window.draw(self.cursor)
