#!/usr/bin/env python3
from SFML import Clock
from src.util.asynccall import call_async
from time import sleep


class Timer(object):
    """docstring for Timer"""

    def __init__(self, target, interval):
        self.target = target
        self.interval = interval
        self.enabled = 1
        self.clock = Clock()

    def start(self):
        call_async(self.__start)
        return self

    def stop(self):
        self.enabled = 0

    def __start(self):
        self.clock.restart()
        while self.enabled:
            time = self.clock.elapsed_time.microseconds
            self.target(time)
            sleep(self.interval)
            self.clock.restart()
