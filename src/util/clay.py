from pickle import dumps
from pickle import loads


class Clay(object):

    def __init__(self, dictionary=None):
        super().__setattr__(
            'properties', [dictionary, dict()][dictionary is None])

    def __getattr__(self, name):
        if name == 'properties':
            return super().__getattr__('properties')
        return self.properties[name]

    def __setattr__(self, name, value):
        if name == 'properties':
            return super().__setattr__('properties', value)
        self.properties[name] = value

    def __getitem__(self, name):
        if type(name) is tuple:
            return tuple(map(self.__getattr__, name))
        else:
            return self.properties[name]

    def __setitem__(self, name, value):
        self.properties[name] = value

    def serialize(self):
        return dumps(self.properties)

    def update(self, dump):
        self.properties = loads(dump)

    def deserialize(dump):
        return Clay(loads(dump))

    def __str__(self):
        return str(self.properties)
