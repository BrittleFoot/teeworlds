#!/usr/bin/env python3


class mKeyEvent(object):

    """docstring for mKeyEvent"""

    def __init__(self, pressed, key):
        self.pressed = pressed
        self.key = key

    def __str__(self):
        return 'KY:%s %s' % (self.pressed, self.key)


class mMouseEvent(object):

    """docstring for mMouseEvent"""

    def __init__(self, pressed, button, position):
        self.pressed = pressed
        self.button = button
        self.position = position

    def __str__(self):
        return 'MB:%s %s %s' % (self.pressed, self.button, self.position)
