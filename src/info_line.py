import SFML as sf

from src.vector_extends import *

from src.fonts import get as fontget
FONT_PATH = fontget('monof55.ttf')


FONT_SIZE = 30
RADIUS = 640


class InfoLine(sf.RectangleShape):

    """ drws info about tee"""

    @classmethod
    def make(cls, window, cursor, info):
        self = cls()
        self.info = info
        self.window = window
        self.size = 0, FONT_SIZE * len(self.info)

        self.text = sf.Text()
        self.text.character_size = 28
        self.text.font = sf.Font.from_file(FONT_PATH)
        self.cursor = cursor

        return self

    def get_alpha(self, tee):
        dist = l(tee.position - self.cursor.position)
        return [int((1 - dist / RADIUS) * 255), 0][dist - RADIUS > 0]

    def get_color(self, type, tee):
        alpha = self.get_alpha(tee)
        return {
            'text': sf.Color(255, 255, 255, alpha),
            'rect': sf.Color(100, 200, 100, int(alpha * 0.7))
        }[type]

    def draw_about(self, tee):
        self.fill_color = self.get_color('rect', tee)
        self.text.color = self.get_color('text', tee)

        pos = tee.position

        res = []

        for string in self.info:
            res.append('%s: %s' % (string, str(getattr(tee, string))))

        self.text.string = '\n'.join(res)

        rect = self.text.global_bounds

        delta = 0,  -1 * tee.global_bounds.height - rect.height
        self.position = self.text.position = tee.position + delta

        self.origin = self.text.origin = rect.size.x / 2, 0

        self.size = rect.size * 1.2
        self.position -= (rect.size.x * 0.1, 0)

        self.window.draw(self)
        self.window.draw(self.text)


def main():
    inline = InfoLine.make(12, ['name', 'health'])


if __name__ == '__main__':
    main()
