#!/usr/bin/env python3
from SFML import Color

from os import path
from src.commands import Func
from src.commands import FuncList
from src.network.commands_codes import CODE
from src.network.commands_codes import SPLITTER

client = None
ostream = None


class ClientInvoker(object):

    """collections of functions"""

    def help(function=None):
        """Helps with console interface and commands.

        With no arguments -  shows list of available commands.
        /help <FUNCTION> - shows help for <FUNCTION>

        HINT: Any functions are accept parameter /?
              Use: /<FUNCTION> /?
        """
        if function:
            send_command(client, '/' + function + ' /?', ostream)
        else:
            ostream(FuncList, Color.YELLOW)
            ostream('List of available functions: \n', Color.YELLOW)

    def send_all(*args):
        """Send message to all.

        Automaticly invoke when console got message with no '/'.
        Alternative usage: /send_all <YOUR MESSAGE HERE>
        """
        client.send(CODE.MESSAGE, ' '.join(args))

    def register(name):
        """Send to server your name for registration.

        Usage: after connect /register <NAME>
        """
        client.send(CODE.NAME, name)

    def connect(ip='127.0.0.1', port=65156):
        """Connect to the server with specific ip and port

        Note: by default ip=127.0.0.1, port=65156
        Usage: /connect [ip] [port]
        """
        client.connect(ip, int(port))

    def respawn(token=None):
        """Send respawn request.

        Note: you just askeing so do not be offended 
            if it isn't happend \\0_0|
        Usage: /respawn
               /respawn canaille! 
        """
        client.send(CODE.RESPAWN, token)

    def admin(password):
        """Ask premissions to be administrator.

        Usage: /admin <password>
        """
        client.send(CODE.ADMIN, password)

    def get(*var):
        """Show info about gven server variable.

        Note: default value of 'var' is ANY,
              it means to get list of varables
        Usage: /get <var>
        """
        if len(var) == 0:
            var = ['ANY']
        client.send(CODE.GET, ' '.join(var))

    def set(*args):
        """Set server variable value.

        Usage: /set <var>=<value> <var...
        """
        client.send(CODE.SET, ' '.join(args))

    def skin(name=''):
        """Set skin for character

        Usage: /skin        # for get list of available skins
               /skin <name> # for get <name> skin and being pretty :3 
        """
        client.send(CODE.SKIN, name)


def decode(command):
    if command and command[0] == '/':
        command = command[1:].split()
        if command:
            cmd = command[0]
            args = command[1:]
            return Func[cmd].name, args
        raise KeyError('')
    else:
        return Func['send_all'].name, command.split()


def send_command(_client, command, _ostream):
    global client
    global ostream

    if not client:
        client = _client
    if not ostream:
        ostream = _ostream

    try:

        func, args = decode(command)

        func = getattr(ClientInvoker, func)

        if args and args[0] == '/?':
            ostream(func.__doc__, Color.YELLOW)
        else:
            func(*args)

    except KeyError as e:
        ostream('Command %s not found.' % e, Color.RED)

    except AttributeError as e:
        eargs = (str(e).rsplit(' ', 1)[-1], path.split(__file__)[-1])
        ostream("Function %s not found in %s" % eargs, Color.RED)

    except TypeError as e:
        ostream(e, Color.RED)


if __name__ == '__main__':
    while 1:
        send_command(1, input(), print)
