#!/usr/bin/env python3
from SFML import Rectangle
from SFML import Texture
from src.tile_types import TILESIZE

IMAGES = 'src/data/images/'


class Tileset(object):

    """docstring for Tileset"""

    def __init__(self, img_name):
        self.img_name = img_name
        self.texture = Texture.from_file(IMAGES + img_name)
        self.x_count = self.texture.width // TILESIZE
        self.y_count = self.texture.height // TILESIZE
        self.max_tid = self.x_count * self.y_count

    def apply_texture(self, tile):
        tid = tile.tid
        if tid >= 0:
            rect = self.calculate_rect(tid)
            tile.set_texture(self.texture, rect)

    def calculate_rect(self, tid):
        x = tid % self.x_count * TILESIZE
        y = tid // self.y_count * TILESIZE

        return Rectangle((x, y), (TILESIZE, TILESIZE))


def main():
    tileset = Tileset('grass_main.png')

    tileset.calculate_rect(15)


if __name__ == '__main__':
    main()
