#!/usr/bin/env python3
from src.network.network_proto import make_command
from src.util.clay import Clay
from struct import Struct

SPLITTER = b'\xE2\xE2\xE8'

CODE = Clay()

CODE.CONNECTED = b'\x01'
CODE.DISCONNECTED = b'\x00'
CODE.UDP_PORT = b'\x02'

CODE.NAME = b'N'
CODE.DEL_NAME = b'n'

CODE.REFRESH = 'r'

CODE.MESSAGE = b'M'
CODE.ERROR = b'E'
CODE.INFO = b'I'
CODE.TEAMMESSAGE = b'T'
CODE.PRIVATE_MESSAGE = 'W'
CODE.SKIN = b'S'

CODE.PRESS = b'P'
CODE.RELEASE = b'p'
CODE.MOUSE = b'm'
CODE.RESPAWN = b'R'

CODE.EVENTS = b'e'
CODE.LEFT = b'l'
CODE.RIGHT = b'r'
CODE.JUMP = b'j'
CODE.LMOUSE = b'\xa0'
CODE.RMOUSE = b'\xa1'
CODE.WHEELUP = b'\xa2'
CODE.WHEELDOWN = b'\xa3'

CODE.ADMIN = b'\xb0'
CODE.SET = b'\xb1'
CODE.GET = b'\xb2'


MOUSEEVENT = Struct('<2i')


KEYS = {CODE.LEFT, CODE.RIGHT, CODE.JUMP}
MOUSES = {CODE.LMOUSE, CODE.RMOUSE}
WHEELS = {CODE.WHEELUP, CODE.WHEELDOWN}

CODE.TEE_DATA = b'\xd0'
CODE.MAP_HEADER = b'\xd1'
CODE.MAP_DATE = b'\xd2'
CODE.MAP_DATE_END = b'\xd3'

CODE.OBJ_HEADER = b'\xd4'
CODE.OBJ_DATA = b'\xd5'
CODE.OBJ_DATA_END = b'\xd6'


def command(*names_of_commands):
    return make_command(*CODE[names_of_commands])


CMD = Clay()

CMD.p_left = command('PRESS', 'LEFT')
CMD.r_left = command('RELEASE', 'LEFT')

CMD.p_right = command('PRESS', 'RIGHT')
CMD.r_right = command('RELEASE', 'RIGHT')

CMD.p_lmouse = command('PRESS', 'LMOUSE')
CMD.r_lmouse = command('RELEASE', 'LMOUSE')

CMD.p_rmouse = command('PRESS', 'RMOUSE')
CMD.r_rmouse = command('RELEASE', 'RMOUSE')

CMD.jump = command('PRESS', 'JUMP')

EVENT_CODES = {CODE.PRESS, CODE.RELEASE}
