#!/usr/bin/env python3
import logging as log
import socket
import time

from concurrent.futures import ThreadPoolExecutor
from queue import Empty
from queue import Queue
from src.network.commands_codes import CODE
from src.network.network_proto import TIMEOUT
from src.network.network_proto import make_command
from src.network.network_proto import recv_command_from
from src.util.asynccall import call_async
from threading import Lock


class Server(object):

    """host the game and answer to users
    """

    def get_id(self):
        self.__id += 1
        return self.__id

    def __init__(self):
        self.main_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.main_socket.settimeout(TIMEOUT)
        self.client_lock = Lock()
        self.__id = 0
        self.pool = ThreadPoolExecutor(max_workers=8)
        self.queue = Queue()

        self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.udp_queue = Queue()

        self.clients = dict()
        self.udpclients = dict()

    def __del__(self):
        self.stop()

    def __bind(self, port):
        while 1:
            try:
                self.main_socket.bind(('', port))
            except OSError as e:
                log.error(e)
                port += 1
            else:
                break
        self.port = port
        log.info('binding on port {}'.format(port))
        self.main_socket.listen(10)

    def start(self, port=65156):
        self.__bind(port)
        self.enabled = True
        call_async(self.__accept)
        call_async(self.__answer_factory)
        call_async(self.nonblocking_answer_factory)

    def stop(self):
        self.enabled = False
        self.main_socket.close()

    def __accept(self):
        while self.enabled:
            try:
                sock, addres = self.main_socket.accept()
                sock.settimeout(1 / 60)
            except socket.timeout:
                continue
            except OSError as e:
                log.error(e)
                break

            call_async(self.client_handler, sock)

    def kick(self, client_id):
        with self.client_lock:
            client_socket = self.clients.pop(client_id)
            client_socket.close()
            udp_sock, addr = self.udpclients.pop(client_id)
            udp_sock.close()
        log.info('Disconnected %d!' % client_id)
        self.async_handle(CODE.DISCONNECTED, None, client_id)

    def client_handler(self, client_socket):

        client_id = self.get_id()

        with self.client_lock:
            self.clients[client_id] = client_socket
        log.info('Connected %d:%s!' % (client_id, client_socket.getpeername()))
        self.async_handle(CODE.CONNECTED, None, client_id)

        try:
            while self.enabled:
                try:
                    command, args = recv_command_from(client_socket)
                except socket.timeout:
                    continue

                if command == CODE.UDP_PORT:
                    with self.client_lock:
                        port = int(args.decode())
                        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                        addr = client_socket.getpeername()[0], port
                        self.udpclients[client_id] = sock, addr
                else:
                    #####################################
                    self.async_handle(command, args, client_id)
                    #####################################

        except (ConnectionResetError, ConnectionAbortedError, OSError) as e:
            log.error(e)
        finally:
            # disconnect client
            self.kick(client_id)

    def async_handle(self, command, args, client_id):
        self.pool.submit(self.handle, command, args, client_id)

    def handle(self, command, args, client_id):
        raise NotImplementedError('Handling wants to be overrriden :-)')

    def __answer_factory(self):

        def send(tuple):
            id, sock = tuple
            try:
                sock.send(cmd)
            except socket.timeout:
                pass
            except OSError as e:
                log.error('Warning with client %s\n%s ' % (id, e))
            except Exception as e:
                log.error(e)

        while self.enabled:
            try:
                cmd = self.queue.get(timeout=TIMEOUT)
            except Empty:
                continue

            clients = self.clients.items()

            with ThreadPoolExecutor(max_workers=8) as e:
                with self.client_lock:
                    e.map(send, clients)

    def nonblocking_answer_factory(self):

        def send(args):
            try:
                client_info, msg = args
                sock, addr = client_info
                sock.sendto(msg, addr)
            except Exception as e:
                print(e)

        while self.enabled:
            try:
                cmd = self.udp_queue.get(timeout=TIMEOUT)
            except Empty:
                continue

            with self.client_lock:
                clients = [(c, cmd) for c in self.udpclients.values()]

            # -- i guess it is too heavy for python to use Executor?
            # with ThreadPoolExecutor(max_workers=8) as e:
            #     e.map(send, clients)

            for args in clients:
                send(args)

    def send_to(self, client_id, command, args=None):
        call_async(self.async_send_to(client_id, command, args))

    def async_send_to(self, client_id, command, args=None):
        cmd = make_command(command, args)
        try:
            self.clients[client_id].send(cmd)
        except (socket.timeout, KeyError):
            pass
        except OSError as e:
            log.error('Warning with client %s\n%s ' % (client_id, e))

    def send_to_anyone(self, command, args=None):
        self.queue.put(make_command(command, args))

    def send_all_noblock(self, command, args=None):
        self.udp_queue.put(make_command(command, args))
