#!/usr/bin/env python3
import logging as log
import socket
import time

from src.network.commands_codes import CODE
from src.network.network_proto import TIMEOUT
from src.network.network_proto import make_command
from src.network.network_proto import recv_command_from
from src.network.network_proto import recv_command_udp
from src.util.asynccall import call_async
from threading import Lock


class Client(object):

    """client for communicate with server"""

    def __init__(self):
        self.socket = socket.socket()
        self.reciver = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.reciver.settimeout(TIMEOUT)
        self.enabled = 1
        self.data_lock = Lock()
        self.new_data = []
        self.connected = 0

    def __del__(self):
        self.stop()

    def bind(self):
        port = 54145
        while 1:
            try:
                self.reciver.bind(('', port))
            except OSError as e:
                log.error(e)
                port += 1
            else:
                break
        log.info('udp binding on port {}'.format(port))
        self.port = port

    def stop(self):
        self.socket.close()
        self.enabled = 0

    def connect(self, ip, port=65156):
        if not self.connected:
            try:
                self.socket = socket.socket()
                self.socket.connect((ip, port))
                call_async(self._getdata)
                call_async(self._getudpdata)
                log.info('Connected to %s!' % ip)
                self.connected = 1
            except (ConnectionRefusedError, OSError) as e:
                log.error(e)
                
            self.send(CODE.UDP_PORT, str(self.port))

    def _getdata(self):
        try:
            while self.enabled:
                try:
                    command, args = recv_command_from(self.socket)
                except socket.timeout:
                    continue

                self.handle(command, args)

        except (ConnectionResetError, ConnectionAbortedError, OSError) as e:
            self.connected = 0
            log.error(e)

    def _getudpdata(self):
        try:
            while self.enabled:
                try:
                    command, args = recv_command_udp(self.reciver)
                except socket.timeout:
                    continue

                self.handle(command, args)

        except (ConnectionResetError, ConnectionAbortedError, OSError) as e:
            self.connected = 0
            log.error(e)

    def async_handle(self, command, args):
        call_async(self.handle, command, args)

    def handle(self, command, args):
        raise NotImplementedError('Handling wants to be overrriden :-)')

    def send(self, bytecommand, args=None):
        if self.connected:
            if self.enabled:
                self.send_noc(make_command(bytecommand, args))
            else:
                self.connected = 0
                raise IOError('Client closen')

    def send_noc(self, bytemessage):
        """ No condition send """
        if not isinstance(bytemessage, bytes):
            raise TypeError('bytemessage requries "bytes" type')

        if self.enabled:
            try:
                self.socket.send(bytemessage)
            except (ConnectionResetError,
                    ConnectionAbortedError,
                    OSError) as e:
                self.connected = 0
                log.error(e)

        else:
            self.connected = 0
            raise IOError('Client closen')
