#!/usr/bin/env python3
import SFML as sf


class StaticRect(sf.RectangleShape):
    """docstring for StaticRect"""

    def __init__(self):
        super(Weapon, self).__init__()
        pass

    def make(smart_view, position=(0, 0), size=(0, 0)):
        self = StaticRect()

        self.view = smart_view
        self.position = position
        self.size = size

        self.start_pos = position

        self.content = None

        return self

    def set_pos(self, position):
        self.start_pos = position

    def update(self, time=0):
        self.position = self.view.corner + self.start_pos

        if self.content is not None:
            self.content.position = self.position + self.size * 0.1

    def draw(self, window):
        window.draw(self)
        if self.content is not None:
            window.draw(self.content)

    def append(self, transformable_drawable):
        self.content = transformable_drawable
