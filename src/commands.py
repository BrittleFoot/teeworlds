#!/usr/bin/env python3
from os.path import join


class NAPare():

    """docstring for NAPare"""

    def __init__(self, name, alevel):
        self.name = name
        self.alevel = alevel

    def __hash__(self):
        return(hash(name))

    def __eq__(self, other):
        return self.name == other.name


Func = dict()

file = open(join('src', 'func_bindings.cfg'))

for line in file.read().split('\n'):
    if line and line[0] != '#':
        try:
            aliases, name, alevel = line.split()

            Func[name] = NAPare(name, alevel)

            for alias in aliases.split(';'):
                Func[alias] = NAPare(name, alevel)

        except ValueError as e:
            print(e)
file.close()

FuncList = ''

i = 0
__k = sorted(Func.keys())
__l = len(__k)
for f in __k:
    FuncList += f
    if i and i % 5 == 0:
        FuncList += '\n'
    elif i != __l - 1:
        FuncList += ', '
    i += 1
