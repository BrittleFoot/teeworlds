"""Some Game logick methods.

    Spawn tee,
    Score counting,
    Ammo respawn thread,
    etc.

"""
import src.ammo as ammo
import src.tile_types as TILE

from random import choice
from src.physic_primtives import cfg
from src.tee import Tee


def spawn_tee(obj_manager, name, args):
    pos = choice(list(obj_manager.level.get_where(TILE.SPAWN))).position

    if name in obj_manager.objects and args != b'canaille!':
        return (False, 'already exist')

    if args == b'canaille!':
        obj = obj_manager.objects.get(name, None)
        if obj is not None and hasattr(obj, 'hit'):
            obj.hit(1000, 'god')

    ball = Tee()
    ball.F += (0, cfg['GRAVITY_FORCE'])
    ball.name = name
    ball.position = pos

    hammer = ammo.Hammer((0, 0))
    gun = ammo.Gun((0, 0))
    granader = ammo.Granader((0, 0))
    shotgun = ammo.Shotgun((0, 0))

    obj_manager.add(ball)
    obj_manager.add_weapon(hammer)
    obj_manager.add_weapon(gun)
    obj_manager.add_weapon(granader)
    obj_manager.add_weapon(shotgun)

    ball.pickup_weapon(hammer)
    ball.pickup_weapon(gun)
    ball.pickup_weapon(granader)
    ball.pickup_weapon(shotgun)
    return True, name
