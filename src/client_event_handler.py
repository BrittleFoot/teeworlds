#!/usr/bin/env python3
from SFML import Keyboard as key
from os.path import join
from src.network.commands_codes import CMD
from src.network.commands_codes import CODE
from src.network.commands_codes import MOUSEEVENT
from src.network.commands_codes import SPLITTER
from src.tee import weapon_number
from src.util.timer import Timer


SEND_E_FREQ = 1 / 33
MPOS_COOLDOWN = 30000


class ClientEventHandler(object):

    """docstring for ClientEventHandler"""

    def __init__(self, send_func, noc_send_func, cursor):
        self.cursor = cursor
        self.send = send_func
        self.noc_send = noc_send_func  # no condition send
        self.enabled = 0

        self.events = []
        self.send_event_timer = Timer(self.send_events, SEND_E_FREQ)

        with open(join('src', 'key_bindings.cfg')) as f:
            conf_key = {l.split()[0]: l.split()[1]
                        for l in f.read().split('\n')}

        def get_code(string):
            try:
                return getattr(key, conf_key[string])
            except AttributeError:
                pass

        self.controll = {get_code(x): x for x in conf_key}
        self.codes = set(self.controll.values())

        self.cooldown = 0

    def start(self):
        self.enabled = 1
        self.send_event_timer.start()

    def stop(self):
        self.send_event_timer.enabled = self.enabled = 0

    def check_key(self, kevent, time, pressed):
        if not self.enabled:
            return

        suffix = [CODE.RELEASE, CODE.PRESS][pressed]

        code = kevent['code']

        if code in self.controll:
            controll = self.controll[code]

            if controll == 'left':
                self.events.append(suffix + CODE.LEFT)

            if controll == 'right':
                self.events.append(suffix + CODE.RIGHT)

            if controll == 'jump' and pressed:
                self.events.append(suffix + CODE.JUMP)

        if code in weapon_number:
            self.events.append(suffix + bytes((code,)))

    def send_events(self, time):
        events = self.events
        self.events = []

        if events:
            self.send(CODE.EVENTS, SPLITTER.join(events))

    def check_mouse_move(self, mevent, time):
        self.cooldown += time
        if self.cooldown > MPOS_COOLDOWN:
            self.cooldown = 0

            suffix = CODE.RELEASE
            button = b'\x00'
            cx, cy = self.cursor.position
            cx, cy = int(cx), int(cy)
            position = MOUSEEVENT.pack(cx, cy)

            self.events.append(suffix + button + position)

    def press_mouse_button(self, button, time, pressed):
        suffix = [CODE.RELEASE, CODE.PRESS][pressed]
        button = [CODE.LMOUSE, CODE.RMOUSE][button]

        cx, cy = self.cursor.position
        cx, cy = int(cx), int(cy)
        position = MOUSEEVENT.pack(cx, cy)

        self.events.append(suffix + button + position)
