#!/usr/bin/env python3
from SFML import Vector2
from SFML import VideoMode
from SFML import View
from math import sqrt
from src.vector_extends import *


def length(vector2):
    return sqrt(vector2.x ** 2 + vector2.y ** 2)


R = 640


class SmartView:

    """docstring for SmartView"""

    def __init__(self, vmode, window, mouse=None):
        self.window = window
        self.vmode = vmode
        self.view = View()
        self.view.center = (0, 0)
        self.view.size = (vmode.width, vmode.height)
        self.target = None
        self.view.size = 1920, 1080
        self.scale = length(self.view.size) / length(self.vmode.size)
        self.ratio = self.view.size / self.vmode.size

        self.mouse = mouse

    def update(self, time):
        self.ratio = self.view.size / self.vmode.size
        self.scale = length(self.view.size) / length(self.vmode.size)

        self.window.view = self.view

        if self.mouse is not None and self.target is not None:
            dist = self.mouse.position - self.target.position
            pos = self.target.position + \
                n(dist) * min(R / 2, max(0, (l(dist) - R) / 2))
            delta = pos - self.view.center

            self.view.center += delta / 2

        elif self.target is not None:
            pos = self.target.position
            dist = pos - self.view.center
            self.view.center += dist / 4

        # to be continued

    @property
    def corner(self):
        return self.view.center - self.view.size / 2

    @property
    def center(self):
        return self.view.center

    @center.setter
    def center(self, vector):
        self.view.center = vector

    @property
    def width(self):
        return self.view.size.x

    @property
    def height(self):
        return self.view.size.y


def main():
    pass


if __name__ == '__main__':
    main()
